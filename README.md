<br />
<p align="center"><a href="https://elated-yonath-b7f1b1.netlify.app/">
  Static Site Preview</a>
  <h3 align="center">Front End Of React Website</h3>

<!-- ABOUT THE PROJECT -->
## About The Project
This is the final project for FS 1030
[![screenshot]]

[![screenshot2]]

[![screenshot3]]

### Notes
* ~~Starting to move ui from reactstrap over to materialUI I prefer the look out of the box~~ Done (did keep the buttons from Reactstrap though)
* so many embeded youtube videos cause delay on page load so I moved everything to a slider and its much better now!
* Added jinke music player and unminified the CSS to a seperate file to be easier to customize
* Decided to throw the MP3 player across the entire site since nobody will want to just sit in one spot to hear a song on the site lol
* Implemented a Blog section the functionality i want out of it is not there yet but hey stuff shows up!
* rework of admin panel still not 100% happy with it but its getting there.
* Managed to get Netlify working with the app ~~but some images are not showing up so still working on that.~~ (Turns out Netlify does not like Opacity)
* Video addition to the blogs Just add a embed Id to the new Blog input area the admin section!
* reworked Password Gen

* CRUD for Admins and blog posts and Delete function for Submission all located in the back admin panel 


  

### Dependencies used
* @material-ui/icons
* @material-ui/core
* @material-ui/lab
* @testing-library/jest-dom
* @testing-library/react
* @testing-library/user-event
* argon2
* bootstrap
* cors
* express
* express-jwt
* jsonwebtoken
* react
* react-bootstrap
* react-dom
* react-player
* react-router-dom
* react-scripts
* reactstrap
* uuid
* styled-components
* pro-gallery
* react-jinke-music-player
* styled-components
* lodash.sortby
* generate-password
* react-reveal


### Installation
1. Install NPM packages
   ```sh
   npm install
   ```
2. Start the App
   ```sh
   npm start
   ```

## Code License
MIT
Copyright 2021 David Duchesneau-Hart

## Music
Music in this repository are Copyright David Duchesneau-Hart ©2021
Do not modify, and/or distribute this music for any purpose with or without fee.




                                                                                   \\\\\\\\\
                                                                                 \\\\\\\\\\\\\
                                                                                \\\\\\\\\\\\\\\
                                                        |-----------,-|           |C>   // )\\\\|
                                                        |        ,','|          /    || ,'/////|
                                                        |---------,','  |         (,    ||   /////
                                                        |        ||    |          \\  ||||//''''|
                                                        |        ||    |           |||||||     _|
                                                        |        ||    |______      `````\____/ \
                                                        |        ||    |     ,|         _/_____/ \
                                                        |        ||  ,'    ,' |        /          |
                                                        |        ||,'    ,'   |       |         \  |
                                                        |_________|/    ,'     |      /           | |
                                                        |_____________,'      ,',_____|      |    | |
                                                        |            |     ,','      |      |    | |
                                                        |            |   ,','    ____|_____/    /  |
                                                        |            | ,','  __/ |             /   |
                                                        |_____________|','   ///_/-------------/   |
                                                        |            |===========,'

[screenshot]: https://i.ibb.co/HxWvrSw/Site.png
[screenshot2]: https://i.ibb.co/WtDfR4R/New-Project-1.png
[screenshot3]: https://i.ibb.co/FKQXjst/admin.png