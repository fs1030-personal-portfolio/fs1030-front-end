import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import PostAddIcon from '@material-ui/icons/PostAdd';
import SpeakerNotesIcon from '@material-ui/icons/SpeakerNotes';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import SubmissionsList from './listings/Submissions';
import UserList from './userlist/list';
import Bloglist from './Blog_Admin_List';
import CU from './pages/CU';
import PwGen from './pwgen/pwgen';
import { Row,Col} from 'reactstrap';
import BlogInput from './Blog_Input';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import EditIcon from '@material-ui/icons/Edit';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-force-tabpanel-${index}`}
      aria-labelledby={`scrollable-force-tab-${index}`}
      {...other}
      classname="adminpanel"
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-force-tab-${index}`,
    'aria-controls': `scrollable-force-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: '#f0f8ff8f',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },

}));

export default function AdminPanel() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          <Tab label="Create New Admin" icon={<SupervisorAccountIcon />} {...a11yProps(0)} />
          <Tab label="Create Blog Post" icon={<PostAddIcon />} {...a11yProps(1)} />
          <Tab label="Edit Blog Post" icon={<EditIcon />} {...a11yProps(2)} />
          <Tab label="Form Submissions" icon={<SpeakerNotesIcon />} {...a11yProps(3)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
            <Row>
                  <Col>
                    <div className={classes.root}>
                      <Accordion>
                        <AccordionSummary
                          expandIcon={<ExpandMoreIcon />}
                          aria-controls="panel1a-content"
                          id="panel1a-header"
                        >
                          <Typography className={classes.heading}>Admin List</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                          <Typography>
                              <UserList/>
                          </Typography>
                        </AccordionDetails>
                    </Accordion>
                    </div>
                  </Col>
            </Row>
            <hr class="yellow"/>
            <Row className="my-5">
                  <Col>
                      <CU/> 
                  </Col>
                  <Col>
                      <PwGen/> 
                  </Col>
            </Row>
      </TabPanel>
      <TabPanel value={value} index={1}>
            <center><h2 className="display-5">Create A New Blog Post</h2></center><br/>
            <hr class="yellow"/>
            <BlogInput/> 
            <hr class="yellow"/>
      </TabPanel>
      <TabPanel value={value} index={2}>
            <center><h2 className="display-5">Edit Blog Post</h2></center><br/>
            <hr class="yellow"/>
            <Bloglist/> 
            <hr class="yellow"/>
      </TabPanel>
      <TabPanel value={value} index={3}>
            <center><h2 className="display-5">Contact Form Submissions</h2></center><br/>
            <hr class="yellow"/>
            <SubmissionsList/> 
            <hr class="yellow"/>
      </TabPanel>
    </div>
  );
}
