import React, { useState } from "react";
import { useHistory } from "react-router";
import Container from "@material-ui/core/Container";
import {
  Form,
  ButtonToggle,
  Row,
  Col,
  FormGroup,
  Label,
  Input,
} from "reactstrap";

const EditUser = (props) => {
  let id = props.match.params.id;
  let EditUser = props.location.state;
  const history = useHistory();
  const token = sessionStorage.getItem("token");
  const [User, setUser] = useState({
    Username: `${EditUser.Username}`,
    password: `${EditUser.password}`,
    Email: `${EditUser.Email}`,
    name: `${EditUser.name}`,
  });

  const handleSubmit = (event) => {
    event.preventDefault();
    fetch(`http://localhost:4000/users/${id}`, {
      method: "PATCH",
      headers: {
        Authorization: `Bearer ${token}`,
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(User),
    }).then((response) => response.json());
    history.push("/submissions");
  };

  const handleChange = (event) => {
    event.persist();
    setUser((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.value,
    }));
  };

  return (
    <div className="main-panel">
        <Container className="containerCU" fixed>
      <Row>
        <Col>
        <center><h3>You are Editing:<i>{User.Username}</i></h3></center><br/>
        </Col>
      </Row>
          <Form onSubmit={(e) => handleSubmit(e)}>
            <Row form>
              <Col md={6}>
                <FormGroup>
                  <Label>Username</Label>
                  <Input
                    type="text"
                    name="Username"
                    id="Username"
                    defaultValue={User.Username}
                    onChange={handleChange}
                  />
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <Label>Password</Label>
                  <Input
                    type="text"
                    name="password"
                    id="password"
                    defaultValue={User.password}
                    onChange={handleChange}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row form>
              <Col md={6}>
                <FormGroup>
                  <Label>Email</Label>
                  <Input
                    type="email"
                    name="Email"
                    id="email"
                    defaultValue={User.Email}
                    onChange={handleChange}
                  />
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <Label>Name</Label>
                  <Input
                    type="text"
                    name="name"
                    id="name"
                    defaultValue={User.name}
                    onChange={handleChange}
                  />
                </FormGroup>
              </Col>
            </Row>
            <ButtonToggle type="submit" color="warning">Submit</ButtonToggle>
          </Form>
        </Container>
    </div>
  );
};

export default EditUser;
