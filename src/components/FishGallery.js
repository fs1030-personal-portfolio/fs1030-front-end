import React from 'react';
import { ProGallery } from 'pro-gallery';
import 'pro-gallery/dist/statics/main.css';

export function FishGallery() {

    // Add your images here...
    const items = [
        { // Image item:
                itemId: '1',
                mediaUrl: './Assets/fishgallery/20.jpg',
                metaData: {
                        type: 'image',
                        height: 150,
                        width: 150
                }
        },
        { // Image item:
                itemId: '2',
                mediaUrl: './Assets/fishgallery/39.jpg',
                metaData: {
                        type: 'image',
                        height: 150,
                        width: 200
                }
        },
        { // Image item:
                itemId: '3',
                mediaUrl: './Assets/fishgallery/38.jpg',
                metaData: {
                        type: 'image',
                        height: 150,
                        width: 300
                }
        },
        { // Image item:
                itemId: '4',
                mediaUrl: './Assets/fishgallery/37.jpg',
                metaData: {
                        type: 'image',
                        height: 150,
                        width: 200
                }
        },
        { // Image item:
                itemId: '5',
                mediaUrl: './Assets/fishgallery/36.jpg',
                metaData: {
                        type: 'image',
                        height: 60,
                        width: 60
                }
        },
        { // Image item:
                itemId: '6',
                mediaUrl: './Assets/fishgallery/35.jpg',
                metaData: {
                        type: 'image',
                        height: 100,
                        width: 200
                }
        },
        { // Image item:
                itemId: '7',
                mediaUrl: './Assets/fishgallery/34.jpg',
                metaData: {
                        type: 'image',
                        height: 100,
                        width: 100
                }
        },
        { // Image item:
                itemId: '8',
                mediaUrl: './Assets/fishgallery/33.jpg',
                metaData: {
                        type: 'image',
                        height: 100,
                        width: 200
                }
        },
        { // Image item:
                itemId: '9',
                mediaUrl: './Assets/fishgallery/10.jpg',
                metaData: {
                        type: 'image',
                        height: 100,
                        width: 200
                }
        },
        { // Image item:
                itemId: '10',
                mediaUrl: './Assets/fishgallery/31.jpg',
                metaData: {
                        type: 'image',
                        height: 100,
                        width: 100
                }
        },
        { // Image item:
                itemId: '11',
                mediaUrl: './Assets/fishgallery/30.jpg',
                metaData: {
                        type: 'image',
                        height: 100,
                        width: 100
                }
        },
        { // Image item:
                itemId: '12',
                mediaUrl: './Assets/fishgallery/29.jpg',
                metaData: {
                        type: 'image',
                        height: 100,
                        width: 200
                }
        },
        { // Image item:
                itemId: '13',
                mediaUrl: './Assets/fishgallery/28.jpg',
                metaData: {
                        type: 'image',
                        height: 100,
                        width: 200
                }
        },
        { // Image item:
                itemId: '14',
                mediaUrl: './Assets/fishgallery/11.jpg',
                metaData: {
                        type: 'image',
                        height: 100,
                        width: 200
                }
        },
        { // Image item:
                itemId: '15',
                mediaUrl: './Assets/fishgallery/27.jpg',
                metaData: {
                        type: 'image',
                        height: 100,
                        width: 200
                }
        },
        { // Image item:
                itemId: '16',
                mediaUrl: './Assets/fishgallery/26.jpg',
                metaData: {
                        type: 'image',
                        height: 100,
                        width: 200
                }
        },
        { // Image item:
                itemId: '17',
                mediaUrl: './Assets/fishgallery/25.jpg',
                metaData: {
                        type: 'image',
                        height: 100,
                        width: 200
                }
        },
        { // Image item:
                itemId: '18',
                mediaUrl: './Assets/fishgallery/24.jpg',
                metaData: {
                        type: 'image',
                        height: 100,
                        width: 100
                }
        },
        { // Image item:
                itemId: '19',
                mediaUrl: './Assets/fishgallery/23.jpg',
                metaData: {
                        type: 'image',
                        height: 100,
                        width: 100
                }
        },
        { // Image item:
                itemId: '20',
                mediaUrl: './Assets/fishgallery/22.jpg',
                metaData: {
                        type: 'image',
                        height: 100,
                        width: 100
                }
        },
        { // Image item:
                itemId: '21',
                mediaUrl: './Assets/fishgallery/21.jpg',
                metaData: {
                        type: 'image',
                        height: 200,
                        width: 200
                }
        },
        { // Image item:
                itemId: '23',
                mediaUrl: './Assets/fishgallery/19.jpg',
                metaData: {
                        type: 'image',
                        height: 50,
                        width: 50
                }
        },
        { // Image item:
                itemId: '24',
                mediaUrl: './Assets/fishgallery/18.jpg',
                metaData: {
                        type: 'image',
                        height: 150,
                        width: 200
                }
        },
        { // Image item:
                itemId: '25',
                mediaUrl: './Assets/fishgallery/17.jpg',
                metaData: {
                        type: 'image',
                        height: 50,
                        width: 50
                }
        },
        { // Image item:
                itemId: '26',
                mediaUrl: './Assets/fishgallery/16.jpg',
                metaData: {
                        type: 'image',
                        height: 50,
                        width: 50
                }
        },
        { // Image item:
                itemId: '27',
                mediaUrl: './Assets/fishgallery/14.jpg',
                metaData: {
                        type: 'image',
                        height: 152,
                        width: 152
                }
        },
        { // Image item:
                itemId: '28',
                mediaUrl: './Assets/fishgallery/13.jpg',
                metaData: {
                        type: 'image',
                        height: 152,
                        width: 152
                }
        },
        { // Image item:
                itemId: '29',
                mediaUrl: './Assets/fishgallery/12.jpg',
                metaData: {
                        type: 'image',
                        height: 50,
                        width: 50
                }
        },
        { // Image item:
                itemId: '30',
                mediaUrl: './Assets/fishgallery/11.jpg',
                metaData: {
                        type: 'image',
                        height: 50,
                        width: 100
                }
        },
        { // Image item:
                itemId: '31',
                mediaUrl: './Assets/fishgallery/5.jpg',
                metaData: {
                        type: 'image',
                        height: 150,
                        width: 150
                }
        },
        { // Image item:
                itemId: '32',
                mediaUrl: './Assets/fishgallery/9.jpg',
                metaData: {
                        type: 'image',
                        height: 100,
                        width: 200
                }
        },
        { // Image item:
                itemId: '33',
                mediaUrl: './Assets/fishgallery/8.jpg',
                metaData: {
                        type: 'image',
                        height: 100,
                        width: 200
                }
        },
        { // Image item:
                itemId: '34',
                mediaUrl: './Assets/fishgallery/7.jpg',
                metaData: {
                        type: 'image',
                        height: 100,
                        width: 200
                }
        },
        { // Image item:
                itemId: '35',
                mediaUrl: './Assets/fishgallery/6.jpg',
                metaData: {
                        type: 'image',
                        height: 100,
                        width: 200
                }
        },
        { // Image item:
                itemId: '36',
                mediaUrl: './Assets/fishgallery/5.jpg',
                metaData: {
                        type: 'image',
                        height: 100,
                        width: 200
                }
        },
        { // Image item:
                itemId: '37',
                mediaUrl: './Assets/fishgallery/4.jpg',
                metaData: {
                        type: 'image',
                        height: 50,
                        width: 50
                }
        },
        { // Image item:
                itemId: '38',
                mediaUrl: './Assets/fishgallery/32.jpg',
                metaData: {
                        type: 'image',
                        height: 50,
                        width: 50
                }
        },
        { // Image item:
                itemId: '39',
                mediaUrl: './Assets/fishgallery/0.jpg',
                metaData: {
                        type: 'image',
                        height: 100,
                        width: 200
                }
        },
        { // Image item:
                itemId: '40',
                mediaUrl: './Assets/fishgallery/1.jpg',
                metaData: {
                        type: 'image',
                        height: 50,
                        width: 50
                }
        },
        { // Image item:
                itemId: '0',
                mediaUrl: './Assets/fishgallery/2.jpg',
                metaData: {
                        type: 'image',
                        height: 50,
                        width: 50
                }
        }
    ]



    // The options of the gallery (from the playground current state)
    const options = {
        galleryLayout: -1,
        scrollAnimation: 'ZOOM_OUT',
        minItemSize: 50,
        scrollDirection: 1,
        imagePlacementAnimation: 'SLIDE',
        itemBorderRadius: 12,
        itemEnableShadow: true,
        itemShadowOpacityAndColor: 'rgba(0,0,0,0.47)',
        overlayBackground: 'rgba(246,242,242,0.11)',
      };
  
      // The size of the gallery container. The images will fit themselves in it
      const container = {
        width: window.innerWidth,
        height: window.innerHeight
      };
  
      // The eventsListener will notify you anytime something has happened in the gallery.
      const eventsListener = (eventName, eventData) => console.log({eventName, eventData}); 
  
      // The scrollingElement is usually the window, if you are scrolling inside another element, suplly it here
      const scrollingElement = window;
  
      return (
        <ProGallery
          items={items}
          options={options}
          container={container}
          eventsListener={eventsListener}
          scrollingElement={scrollingElement}
        />
      );
    }
  
  export default FishGallery