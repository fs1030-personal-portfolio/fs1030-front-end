import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Amps from  './ohm/ohm'
import { Button} from 'reactstrap';

function rand() {
  return Math.round(Math.random() * 1) - 1;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'relative',
    width: 600,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #336699',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function OhmsCalc() {
  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const body = (
    <div style={modalStyle} className={classes.paper}>
      <p id="simple-modal-description">
          <br/>
          <center><img className="Appimg" src="Assets/portfolio/appBanners/ohmL.png" alt="Burning Guitar Animation that says Guitar Name Generator" /></center><br/>
          <p>Ohm's Law is a formula used to calculate the relationship between voltage, current and resistance in an electrical circuit. To students of electronics, Ohm's Law (E = IR) is as fundamentally important as Einstein's Relativity equation (E = mc²) is to physicists.</p>
          <h5>Enter your Voltage(V) & Resistance(Ω) then click Calculate to Get your Amperage(A)</h5>
          <br/>
          <hr class="yellow"/>
        <Amps/> 
          <br/>  
      </p>
    </div>
  );

  return (
    <div align="center">
    <Button type="button" color="warning"  onClick={handleOpen}>
      Ohm's Law Calculator
    </Button><br/><br/>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
}
