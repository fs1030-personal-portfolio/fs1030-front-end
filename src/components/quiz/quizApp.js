import React from "react";
import { QuizData } from "./QuizData.js";
import "./quizstyle.css";
import { Button} from 'reactstrap';

class Quiz extends React.Component {
  state = {
    userAnser: null,
    currentQuestion: 0,
    options: [],
    quizEnd: false,
    score: 0,
    disabled: true
  };

  loadQuiz = () => {
    const { currentQuestion } = this.state;
    this.setState(() => {
      return {
        questions: QuizData[currentQuestion].question,
        options: QuizData[currentQuestion].options,
        answer: QuizData[currentQuestion].answer
      };
    });
  };
  componentDidMount() {
    this.loadQuiz();
  }

  nextQuestionHandler = () => {
    const { userAnswer, answer, score } = this.state;
    this.setState({
      currentQuestion: this.state.currentQuestion + 1
    });
    console.log(this.state.currentQuestion);
    //increase correct answer count
    if (userAnswer === answer) {
      this.setState({
        score: score + 1
      });
    }
  };
  componentDidUpdate(prevProps, prevState) {
    const { currentQuestion } = this.state;
    if (this.state.currentQuestion !== prevState.currentQuestion) {
      this.setState(() => {
        return {
          disabled: true,
          questions: QuizData[currentQuestion].question,
          options: QuizData[currentQuestion].options,
          answer: QuizData[currentQuestion].answer
        };
      });
    }
  }
  //check 
  checkAnswer = (answer) => {
    this.setState({
      userAnswer: answer,
      disabled: false
    });
  };
  finishHandler = () => {
    const { userAnswer, answer, score } = this.state;

    if (this.state.currentQuestion === QuizData.length - 1) {
      this.setState({
        quizEnd: true
      });
      if (userAnswer === answer) {
        this.setState({
          score: score + 1
        });
        console.log("Current score:", this.state.score);
      }
    }
  };

  render() {
    const {
      questions,
      options,
      currentQuestion,
      userAnswer,
      quizEnd
    } = this.state;
    if (quizEnd) {
      return (
        <div>
          <h2>Your final score is {this.state.score} out of {QuizData.length}</h2>
          <p>The correct answer's are: </p>
          <ul>
            {QuizData.map((item, index) => (
              <li className="ui floating message options" key={index}>
                {item.answer}
              </li>
            ))}
          </ul>
        </div>
      );
    }

    return (
      <div className="App">
        <h3>{questions}</h3>
        <span>{` Question ${currentQuestion + 1} out of ${
          QuizData.length
        }`}</span>
        {options.map((option) => (
          <p
            key={option.id}
            className={`ui floating message options
          ${userAnswer === option ? "selected" : null}
          `}
            onClick={() => this.checkAnswer(option)}
          >
            {option}
          </p>
        ))}
        {currentQuestion < QuizData.length - 1 && (
          <Button 
            color="warning"
            className="ui Button black"
            disabled={this.state.disabled}
            onClick={this.nextQuestionHandler}
          >
            Next
          </Button>
        )}
        {currentQuestion === QuizData.length - 1 && (
          <Button color="warning" className="ui Button black" onClick={this.finishHandler}>
            Finish
          </Button>
        )}
      </div>
    );
  }
}

export default Quiz;
