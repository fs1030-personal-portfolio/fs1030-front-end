export const QuizData = [
    {
      id: 0,
      question: ` Who invented the Flaming Moe?`,
      options: [`Homer`, `Moe`, `Barney`, `Carl`],
      answer: `Moe`
    },
    {
      id: 1,
      question: `Who was the president of the US BEFORE Lisa took office`,
      options: [`Arnold Schwarzenegger`, `Donald Trump`, `Fat Tony`, `Dwayne Elizondo Mountain Dew Camacho`],
      answer: `Donald Trump`
    },
    {
      id: 2,
      question: `What class does Moe teach?`,
      options: [`How to have a successful wedding`, `How to get rid of drunks`, `Mixology 101`, `Funk dancing for self-defense`],
      answer: `Funk dancing for self-defense`
    },
    {
      id: 3,
      question: `What's the name of Lisa's substitute teacher?`,
      options: [`Mr. Bergstrom`, `Mr. Crabapple`, `Mr. Hoover`, `Mr. Crowley`],
      answer: `Mr. Bergstrom`
    },
    {
      id: 4,
      question: `Where do all the "schmucks" work?`,
      options: [`In Shelbyville`, `At the glue factory`, `At the box factory`, `On Krusty's program`],
      answer: `At the box factory`
    },
    {
      id: 5,
      question: `What's inside the Sunsphere?`,
      options: [`A scale diagram of Springfield`, `A super computer`, `Al Gore dolls`, `Boxes and boxes of wigs`],
      answer: `Boxes and boxes of wigs`
    },
    {
      id: 6,
      question: `How does Homer regrow his hair?`,
      options: [`With Bolonium`, `With Dimoxinil`, `With Brassafrax`, `With Electromicide`],
      answer: `With Dimoxinil`
    },
    {
      id: 7,
      question: `In season one, episode one, what did Bart want for Christmas?`,
      options: [`A Football`, `A Guitar`, `A tattoo`, `A Chainsaw`],
      answer: `A tattoo`
    }
  ];
  