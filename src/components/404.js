import React from 'react';
import {Button, Container} from 'reactstrap'

const NotFound = () => {
  return <Container><center><img src="Assets/music/404.png" alt="404 Error message Page not found" /><br/><br/>
  <Button color="warning" href="/">Return Home</Button></center>
  </Container>
  ;
}

export default NotFound;