import React, { Component } from 'react';
import { Button, Form,Label, Input,Row,Col,Card} from 'reactstrap'

class Amps extends Component {
	state = {
		resistance: '',
		Amps: '',
		voltage: ''
	}

	handleOnChange = (e) => {
		const name = e.target.name;
		const value = e.target.value;

		if (name === 'voltage') {
			this.setState({voltage: value});
		} else if (name === 'resistance') {
			this.setState({resistance: value});
		}

		e.preventDefault();
	}

	handleCompute = (e) => {
		const v = this.state.voltage;
		const r = this.state.resistance;
		let i = 0.0;

		if (v === '') {
			this.setState({
				voltage: '',
				Amps: '',
				resistance: ''
			});
		} else if (r === '') {
			this.setState({
				voltage: '',
				Amps: '',
				resistance: ''
			});
		} else {
			i += parseFloat(v) / parseFloat(r);
			this.setState({
				Amps: i+'A',
				voltage: '',
				resistance: ''
			});
		}

		e.preventDefault();
	}

	handleClear = (e) => {
		this.setState({
			resistance: '',
			Amps: '',
			voltage: ''
	});

		e.preventDefault();
	}

	render() {
		return (
			<div className="OuterContainer">
				<Form onSubmit={this.handleCompute} onChange={this.handleOnChange}>
					<div className="InnerContainer">
						<div className="Title">
						</div>
						<div className="Inputs">
							<Label><b>Voltage(V)</b></Label>
							<Input
								className="Input"
								type="number"
								name="voltage"
								placeholder="0.0"
								value={this.state.voltage}
							/>
						</div>
						   <div className="Inputs">
							<Label><b>Resistance(Ω)</b></Label>
							<Input
								className="Input"
								type="number"
								name="resistance"
								placeholder="0.0"
								value={this.state.resistance}
							/>
						</div> <hr class="yellow"/>
						<div className="InputsAnswer">
							<Label><b>Amperage(A)</b></Label>
							<Input
								className="InputAnswer"
								type="text"
								name="Amps"
								placeholder="0.0A"
								readonly="true"
								value={this.state.Amps}
							/>
						</div>
                        <br/><br/>
                        <Row>
                            <Col md="4" className="mb-5">
                                <Card>
                                    <Button
                                        className="btn"
                                        type="submit"
                                        name="compute"
                                        value="Compute"
                                        color="warning"
                                        >
                                        Calculate
                                    </Button>
                                </Card>
                            </Col>
                            <Col>
                            </Col>
                            <Col md="4" className="mb-5">
                                <Card>
                                    <Button
                                        className="btn-clear"
                                        color="warning"
                                        onClick={this.handleClear}
                                        >
                                        Clear
                                    </Button>
                                </Card>
                            </Col>
                        </Row>	
					</div>
				</Form>
			</div>
		)
	}
}

export default Amps;