import React from 'react'
import { Container } from 'reactstrap'
import GitHubIcon from '@material-ui/icons/GitHub';
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';
import YouTubeIcon from '@material-ui/icons/YouTube';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import ContactMailIcon from '@material-ui/icons/ContactMail';
import PhoneInTalkIcon from '@material-ui/icons/PhoneInTalk';

const Footer = () => {
    return(
    <footer  style={{backgroundColor: '#336699', opacity: '95%'}}>
        <Container>
            <p className="m-0 text-center text-white">
                Copyright &copy; David Duchesneau-Hart 2021
                <a without rel="noopener noreferrer" target="_blank" class="footericons" href="https://github.com/SpicyDoritos"><GitHubIcon/></a>
                <a without rel="noopener noreferrer" target="_blank" class="footericons" href="/assets/Testdomecert.png"><VerifiedUserIcon/></a>
                <a without rel="noopener noreferrer" target="_blank" class="footericons" href="https://www.youtube.com/channel/UCm_MidX8mZ20QZuAJW6lvBw"><YouTubeIcon/></a>
                <a without rel="noopener noreferrer" target="_blank" class="footericons" href="https://www.linkedin.com/in/ddhkingston/"><LinkedInIcon/></a>
                <a class="footericons" href="mailto:dave@casthaste.com"><ContactMailIcon/></a>
                <a without rel="noopener noreferrer" target="_blank" class="footericons" href="https://discord.gg/FGUaRYFmbM"><PhoneInTalkIcon/></a>
            </p>
        </Container>
    </footer>
  )
}

export default Footer