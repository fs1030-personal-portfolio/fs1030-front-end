import React, { useState } from 'react'
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, Container } from 'reactstrap'
import { NavLink as RouteLink } from 'react-router-dom'
import { useHistory } from "react-router-dom"
import audioLists from '../audiolist';
import BackToTopButton from '../backtotop';
import ReactJkMusicPlayer from "react-jinke-music-player";
 
//Secret
let user_keys = [],
konami = '38,38,40,40,37,39,37,39,66,65';
    document.onkeydown = function(e){
    user_keys.push(e.keyCode)
    if (user_keys.toString().indexOf(konami) >= 0) {
        alert("You found my secret!");
        setTimeout(3000);
        alert("Well what should we do now?");
        setTimeout(3000);
        alert("Why not solo!!!!!");
        setTimeout(3000);
    window.location.href = "/secretpage";
user_keys = [];
}}

      const Navigation = () => {
        let status = sessionStorage.getItem('token') 
        const [isOpen, setIsOpen] = useState(false)
        const toggle = () => setIsOpen(!isOpen)
        const [token, setToken] = useState(status)
        let history = useHistory()
    
        const logout = event => {
            event.preventDefault()
            sessionStorage.removeItem('token')
            setToken(false)
            history.push("/")
        }
    
        if (status){
            setTimeout(function (){
                sessionStorage.removeItem('token')
                setToken(false)
                history.push("/login")
                setTimeout(function(){
                    alert("Quit AFKing for so long, log back in to continue you session.")
                }, 2000)          
            }, 8000*10*10) 
          }

    return (
        <Navbar style={{backgroundColor: '#336699', opacity: '95%'}} expand="md" fixed="top">
            <Container>
                <div className="musicplayer">
                    <ReactJkMusicPlayer audioLists={audioLists}  showMediaSession autoPlay={false} volumeFade={{ fadeIn: 500, fadeOut: 500 }} showDownload={false} defaultVolume={0.5}/>
                </div>
            <NavbarBrand href=""><img className="navimg" src="./Assets/me.jpg" alt="David Duchesneau-Hart" /></NavbarBrand>
            <NavbarToggler onClick={toggle} />
            <Collapse isOpen={isOpen} navbar>
                <Nav className="ml-auto" navbar>
                    <NavItem>
                        <NavLink tag={RouteLink} to="/">Home</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink tag={RouteLink} to="/Resume">Resume</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink tag={RouteLink} to="/Portfolio">Web Portfolio</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink tag={RouteLink} to="/music">Music</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink tag={RouteLink} to="/fishroom">Fish Room</NavLink>
                    </NavItem>
                    <NavItem>
                       <NavLink tag={RouteLink} to="/blog">Blog</NavLink>
                    </NavItem>
                    <NavItem>
                       <NavLink tag={RouteLink} to="/contact">Contact Me</NavLink>
                    </NavItem>
                    {token
                    ? (
                    <><NavItem className="private">
                        <NavLink tag={RouteLink} to="/submissions" >Admin</NavLink>
                    </NavItem>
                    <NavItem className="private">
                        <NavLink tag={RouteLink} to="/" onClick={logout}>Logout</NavLink>
                    </NavItem></>
                      )
                    : (
                    <NavItem>
                        <NavLink tag={RouteLink} to="/login">Login</NavLink>  
                     </NavItem>
                      )          
                    }           
                </Nav>
            </Collapse>
                <BackToTopButton />
            </Container>
            
        </Navbar>
    )
}

export default Navigation