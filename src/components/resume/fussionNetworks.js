import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import FussionNetworks from './modal/5';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

export default function Fussion() {
  const classes = useStyles();
  
  return (
    <Card className={classes.root}>
      <CardHeader
        title="Fussion-Networks"
        subheader="Co-Founder/Owner"
      />
      <CardMedia
        className={classes.media}
        image="./assets/resume/fnlogo.png"
        title="Fussion-Networks"
        alt="Fussion-Networks"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
        <i>&quot;Fussion-Networks was the first north american Dekaron Private server open the public with over 28k players at its peak.&quot;</i><br/><br/><br/>
            <FussionNetworks/>
        </Typography>
      </CardContent>
    </Card>
  );
}
