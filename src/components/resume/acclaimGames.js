import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import AcclaimGames from './modal/6';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

export default function Acclaim() {
  const classes = useStyles();
  
  return (
    <Card className={classes.root}>
      <CardHeader
        title="Acclaim Games INC"
        subheader="Game Master/ Database Analyst"
      />
      <CardMedia
        className={classes.media}
        image="./assets/resume/Acc.png"
        title="Acclaim Games"
        alt="Acclaim Games"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
        <i>&quot;Acclaim Games Incorporated was an American video game company. The company was founded in 2006 and was the successor to Acclaim Entertainment in terms of brand name.&quot;</i><br/><br/>
            <AcclaimGames/>
        </Typography>
      </CardContent>
    </Card>
  );
}
