import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import VolunteerAbroad from './modal/7';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

export default function Volunteer() {
  const classes = useStyles();
  
  return (
    <Card className={classes.root}>
      <CardHeader
        title="Volunteer Abroad"
        subheader="Junior Web Developer"
      />
      <CardMedia
        className={classes.media}
        image="./assets/resume/VA.png"
        title="Volunteer Abroad"
        alt="Volunteer Abroad"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
        <i>&quot;We have been connecting international volunteers with organizations overseas in need of assistance since 1998. We are one of the world's longest running, most flexible and most affordable short-term volunteer programs.&quot;</i><br/><br/>
            <VolunteerAbroad/>
        </Typography>
      </CardContent>
    </Card>
  );
}
