import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import TravelCuts from './modal/8';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

export default function Travel() {
  const classes = useStyles();
  
  return (
    <Card className={classes.root}>
      <CardHeader
        title="Travel Cuts"
        subheader="Junior Web Developer"
      />
      <CardMedia
        className={classes.media}
        image="./assets/resume/TC.svg"
        title="Travel Cuts"
        alt="Travel Cuts"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
        <i>&quot;Travelcuts is a Canadian travel agency specializing in youth travel. It is owned by Merit Travel Group. It operates 19 locations in Canada, often on or near university and college campuses.&quot;</i><br/><br/>
            <TravelCuts/>
        </Typography>
      </CardContent>
    </Card>
  );
}
