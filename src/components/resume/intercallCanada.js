import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import IntercallCanada from './modal/4';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

export default function Intercall() {
  const classes = useStyles();
  
  return (
    <Card className={classes.root}>
      <CardHeader
        title="Intercall"
        subheader="Account Manager"
      />
      <CardMedia
        className={classes.media}
        image="./assets/resume/Iclogo.png"
        title="Intercall Canada"
        alt="Intercall Canada"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
        <i>&quot;InterCall, a subsidiary of Intrado, is an Chicago based audio and web conferencing services provider.&quot;</i><br/><br/><br/>
            <IntercallCanada/>
        </Typography>
      </CardContent>
    </Card>
  );
}
