import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CastHasteLLC from './modal/1';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

export default function Cast() {
  const classes = useStyles();
  
  return (
    <Card className={classes.root}>
      <CardHeader
        title="CastHaste LLC"
        subheader="Senior Web Developer"
      />
      <CardMedia
        className={classes.media}
        image="./assets/resume/CH.png"
        title="CastHaste LLC"
        alt="CastHaste LLC"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
        <i>&quot;We build, fix, and foster digital ecosystems for growing businesses that need it right the first time.&quot;</i><br/><br/>
            <CastHasteLLC/>
        </Typography>
      </CardContent>
    </Card>
  );
}
