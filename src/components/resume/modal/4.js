import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import { Button} from 'reactstrap';

function rand() {
  return Math.round(Math.random() * 1) - 1;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'relative',
    width: 600,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #336699',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function IntercallCanada() {
  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const body = (
    <div style={modalStyle} className={classes.paper}>
      <img className="resumeimg" src="./Assets/resume/intercall.png" alt="" />
      <h3 className="display-4">Intercall Canada</h3>
      <h3 className="display-6">Account Manager</h3>
      <p className="lead">April 2009 - Jan 2015</p>
      <p className="lead">At Intercall I managed fortune 500 accounts for Voip Services</p>
      <hr className="my-2" />
      <p class="card-text">• Developed an internal site to consolidate procedures & systems for customer service<br/> • Created video demo’s for internal systems & customer service training<br/> • Manage Voip Accounts<br/> • oversaw Voip Cisco bridges<br/> • Team spirit Award & employee of the quarter<br/></p>
      <p className="lead">
      </p>
    </div>
  );

  return (
    <div align="center">
      <Button type="button" color="warning"  onClick={handleOpen}>
        Read More
      </Button><br/><br/>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
}
