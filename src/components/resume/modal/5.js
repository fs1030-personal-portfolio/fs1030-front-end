import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import { Button} from 'reactstrap';

function rand() {
  return Math.round(Math.random() * 1) - 1;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'relative',
    width: 600,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #336699',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function FussionNetworks() {
  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const body = (
    <div style={modalStyle} className={classes.paper}>
      <img className="resumeimg" src="./Assets/resume/DK.png" alt="" />
      <h3 className="display-4">Fussion-Networks</h3>
      <h3 className="display-6">Co-Founder/Developer</h3>
      <p className="lead">Dec 2007 - Dec 2013</p>
      <p className="lead">fussion-networks was founded in 2007 as a private game server host and development company,with over 28 thousand players at our peek on multiple servers from dekaron, gunz, lineage 2, rose online just to name a few!<br/>
                            At the end of the 2011 We revamped fussion into a game development company and closed our game servers & created 3 small games and a set of missions for arma 2.<br/>At the end of 2016 I closed our doors due to lack of funding<br/></p>
      <hr className="my-2" />
      <p class="card-text">•Created the first North American private Dekaron Server<br/>•Created/maintained 6 Clusters of Game servers<br/>•Managed very large Community<br/>•Programmed anti hacking methods through DLL<br/>•Managed payment service backend<br/>•Managed the global team of 15 Game Masters<br/>•Created many web interface systems<br/>•Created 3 templates for the unity app store<br/></p>
      <p className="lead">
      </p>
    </div>
  );

  return (
    <div align="center">
      <Button type="button" color="warning"  onClick={handleOpen}>
        Read More
      </Button><br/><br/>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
}
