import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import { Button} from 'reactstrap';

function rand() {
  return Math.round(Math.random() * 1) - 1;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'relative',
    width: 600,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #336699',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function CataclysmServers() {
  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const body = (
    <div style={modalStyle} className={classes.paper}>
      <img className="resumeimg" src="./Assets/resume/Cat.png" alt="" />
      <h3 className="display-4">Cataclysm Servers</h3>
      <h3 className="display-6">Founder/Developer</h3>
      <p className="lead">Nov 2020 - April 2020</p>
      <p className="lead">I had created Cataclysm Servers to host game servers and create unreal 4 projects for Conan exiles and various other games. I hope to bring back the hay day of classic MMO style games with a survival twist Our mod currently has over 300 subscriptions on steam. I was in charge of taking care of the following:</p>
      <hr className="my-2" />
      <p class="card-text">• Host and Maintain Game Servers<br/>• Unreal Engine 4 Map Creation<br/>• Steam Workshop<br/>• Website Creation<br/>• 3d Modeling<br/>• Unreal 4 blueprints<br/>• Discord<br/></p>
      <p className="lead">
      </p>
    </div>
  );

  return (
    <div align="center">
      <Button type="button" color="warning"  onClick={handleOpen}>
        Read More
      </Button><br/><br/>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
}
