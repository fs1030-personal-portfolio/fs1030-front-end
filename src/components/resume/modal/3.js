import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import { Button} from 'reactstrap';

function rand() {
  return Math.round(Math.random() * 1) - 1;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'relative',
    width: 600,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #336699',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function VapoursCanada() {
  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const body = (
    <div style={modalStyle} className={classes.paper}>
        <img className="resumeimg" src="./Assets/resume/VC.png" alt="" />
        <h3 className="display-4">Vapours Canada</h3>
        <h3 className="display-6">Manager</h3>
        <p className="lead">Sept 2016 - Nov 2020</p>
        <p className="lead">I created & maintained Infrastructure systems while I managed the most profitable store in the chain.</p>
        <hr className="my-2" />
        <p class="card-text">• Developed, rolled out &amp; maintained a ERP system for 6 retail stores for Manufacturing, Warranty &amp; day to day operations<br/>• Opened 2 new retail locations (from building procurement to contractors to final touch's)<br/>• Administered back end systems<br/>• Created &amp; Maintained SharePoint Systems<br/>• Created &amp; Maintained media in store&nbsp;<br/>• Maintained SOP and training materials<br/>• Trained new staff<br/>• Scheduling<br/>• Deposits / money management<br/>• Reconciliation of assets<br/></p>
         <p className="lead">
         </p>
    </div>
  );

  return (
    <div align="center">
      <Button type="button" color="warning"  onClick={handleOpen}>
        Read More
      </Button><br/><br/>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
}
