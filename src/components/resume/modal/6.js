import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import { Button} from 'reactstrap';

function rand() {
  return Math.round(Math.random() * 1) - 1;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'relative',
    width: 600,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #336699',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function AcclaimGames() {
  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const body = (
    <div style={modalStyle} className={classes.paper}>
      <img className="resumeimg" src="./Assets/resume/Acc.png" alt="" />
      <h3 className="display-4">Acclaim Games</h3>
      <h3 className="display-6">Game Master/Database Analyst</h3>
      <p className="lead">Dec 2006- Nov 2007</p>
      <p className="lead">I worked for Acclaim Games on the game called 2Moons as a Database Analyst & Game Master. <br/>Later into my career I helped created and manage the Mystery Men a group of coders/developers where we reverse engineered private & public sourced hacks and updated our server end scripts/database to prevent them being used. During my tenure I also caught over 3200 hackers.</p>
      <hr className="my-2" />
      <p class="card-text">• Patrolled in Game and via Database to catch hackers & note trends<br/>• Managed Mystery Men<br/>• Reverse engineered "hacks"<br/>• Ran and created SQL scripts to catch hackers<br/>• Log reviews<br/></p>
      <p className="lead">
      </p>
    </div>
  );

  return (
    <div align="center">
      <Button type="button" color="warning"  onClick={handleOpen}>
        Read More
      </Button><br/><br/>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
}
