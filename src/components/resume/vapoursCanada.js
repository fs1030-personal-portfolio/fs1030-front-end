import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import VapoursCanada from './modal/3';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 600,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

export default function Vapours() {
  const classes = useStyles();
  
  return (
    <Card className={classes.root}>
      <CardHeader
        title="Vapours Canada"
        subheader="Store Manager"
      />
      <CardMedia
        className={classes.media}
        image="./Assets/resume/VC.jpg"
        title="Vapours Canada"
        alt="Vapours Canada"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
            <i>&quot;Vapours Canada is one of the leading e-cigarette retail chains in Ontario. With 6 vape stores across Ontario.&quot;</i><br/><br/>
            <VapoursCanada/>
        </Typography>
      </CardContent>
    </Card>
  );
}
