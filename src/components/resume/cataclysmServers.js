import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import CataclysmServers from './modal/2';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

export default function CataclysmS() {
  const classes = useStyles();
  
  return (
    <Card className={classes.root}>
      <CardHeader
        title="Cataclysm Servers"
        subheader="Founder/Developer"
      />
      <CardMedia
        className={classes.media}
        image="./assets/resume/1.png"
        title="Cataclysm Servers"
        alt="Cataclysm Servers"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
        <i>&quot;Cataclysm offers a unique experience to players willing to commit to harsh conditions. The server was opened to the public in mid-November of 2020.&quot;</i>
            <CataclysmServers/>
        </Typography>
      </CardContent>
    </Card>
  );
}
