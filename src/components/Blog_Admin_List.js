import React, { useEffect, useState } from "react";
import parseJwt from "../helpers/authHelper";
import {Container, Button, Row, Media } from "reactstrap";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import EditIcon from "@material-ui/icons/Edit";
import { useHistory } from "react-router";
import Tooltip from '@material-ui/core/Tooltip';


const Bloglist = () => {
  const token = sessionStorage.getItem("token");
  const Blog = parseJwt(token).Blogname;
  const [Blogs, setBlogs] = useState([]);
  const history = useHistory();

  useEffect(() => {
    const getData = async () => {
      const response = await fetch("http://localhost:4000/blog/entries/", {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      const data = await response.json();
      setBlogs(data);
    };
    getData();
  }, [token]);

  const BlogEditRoute = (event, Blog) => {
      event.preventDefault();
      let path = `/blog/entries/${Blog.blogID}`
      history.push(path, Blog);
  }

  const BlogDelete = async (event, Blog) => {
    event.preventDefault()
    console.log(Blog)
    const response = await fetch(`http://localhost:4000/blog/entries/${Blog.blogID}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`,
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    })
    const data = await response.json();
    console.log(data)
    window.location.reload();
  }

  return (
    <Container  classname="containerCU">
      <Row className="BlogTitle">
        <h2 className="display-5">
          Total Blogs:{Blogs.length}
          {Blog}
        </h2>
      </Row>
      
          {Blogs.length === 0 && (
                <i>No Blogs found</i>
          )}
          {Blogs.length > 0 &&
            Blogs.map((Blog) => (
              
              <Media>
                <Media>
                  <img src="https://i.ibb.co/wgyvPtS/New-Project-1.png" alt="blog edit"></img>
                </Media>
                <Media body>
                  <Media heading>
                  <b>Blog Title:</b>{Blog.Title}
                  </Media>
                  <b>Subject:</b> {Blog.subject} <br/><br/>
                  <b>Content:</b> {Blog.content} <br/><br/>
                  <b>VideoID:</b> {Blog.VideoID} <br/><br/>
                  <Tooltip title="Click here to Edit the Blog">
                  <Button color="warning" onClick={(e) => BlogEditRoute(e, Blog)}>
                    <EditIcon />
                  </Button>
                  </Tooltip>
                  <Tooltip title="Click here to Delete the Blog">
                  <Button color="danger" onClick={(e) => BlogDelete(e, Blog)}>
                    <DeleteForeverIcon />
                  </Button>
                  </Tooltip>
            <hr class="yellow"/>
                </Media>
              </Media>
            ))}
    </Container>
  );
};

export default Bloglist;
