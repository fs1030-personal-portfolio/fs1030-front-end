import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { Button} from 'reactstrap';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

export default function Cataclysm() {
  const classes = useStyles();
  
  return (
    <Card className={classes.root}>
      <CardHeader
        title="Cataclysm Servers"
      />
      <CardMedia
        className={classes.media}
        image="./assets/portfolio/1.png"
        title="Cataclysm Servers"
        alt="Cataclysm Servers"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
            I created Cataclysm Servers to host game servers and create unreal 4 projects for Conan exiles,I used Unreal Engine 4 to create a whole new area, creatures, items etc.<p/>Between the 2 mods we had 300+ subscribers.<br/><br/><br/><br/><br/><br/>
            <br/>
            <Button class="bottombuttons" color="warning" href="http://cataclysm.duchmorri.com/" target="_blank">Website</Button>  <Button color="warning"   href="https://gitlab.com/dduchesn/cataclysmservermod2" target="_blank">Mod Source</Button>
        </Typography>
      </CardContent>
    </Card>
  );
}
