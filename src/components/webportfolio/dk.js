import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

export default function Cataclysm() {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardHeader
        title="Fussion-networks"
        subheader="Large Scale Game Server"
      />
      <CardMedia
        className={classes.media}
        image="./assets/portfolio/4.png"
        title="Large Scale Game Server"
        alt="Large Scale Game Server Fussion Networks"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
            In 2007 I opened the first North American Dekaron private server under the name Fussion Dekaron. At its peak we had approx 28K players.<br/><br/><br/>
            <br/>
        </Typography>
      </CardContent>
    </Card>
  );
}
