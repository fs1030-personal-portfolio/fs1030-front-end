import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { Button} from 'reactstrap';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

export default function Wasteland() {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardHeader
        title="Desolate-Wasteland"
      />
      <CardMedia
        className={classes.media}
        image="./assets/portfolio/2.jpg"
        title="Fussion-Wasteland"
        alt="Fussion-Wasteland"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
            I created the Desolate-Wasteland mission that got pretty popular in Arma II on PC it had many custom coded features in ASL/SQF a C like language.<p/>The source can be found below.<br/><br/><br/><br/><br/><br/>
            <br/>
            <Button class="bottombuttons" color="warning"  href="https://github.com/Fackstah/FussionWasteland_Nearfinal" target="_blank">Mod Source</Button>
        </Typography>
      </CardContent>
    </Card>
  );
}
