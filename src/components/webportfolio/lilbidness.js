import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

export default function Lil() {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardHeader
        title="Just a Lil Bidness"
        subheader="HTML/CSS/PHP/JS"
      />
      <CardMedia
        className={classes.media}
        image="./assets/portfolio/bidness.png"
        title="Just a Lil Bidness"
        alt="Just a Lil Bidness"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
            The other Co-Founder of Fussion Dekaron needed a website for her new estate bidding business this was created circa 2009.<br/><br/><br/><br/>
            <br/>
        </Typography>
      </CardContent>
    </Card>
  );
}
