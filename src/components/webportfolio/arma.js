import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

export default function Arma() {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardHeader
        title="Arma 3 Mission Set"
        subheader="ASL/SQF"
      />
      <CardMedia
        className={classes.media}
        image="./assets/portfolio/arma.gif"
        title="Arma Mission Set"
        alt="Arma Mission Set"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
            When arma 3 came out I moved My version of wasteland over to it and also made a set of 20 missions as a seperate pack.<br/>I also created the first Go-kart mission in Arma 3 called ArmaKart 64 that gave Bohemia the idea to Create a whole DLC called Arma 3 Karts.<br/>
            <br/>
        </Typography>
      </CardContent>
    </Card>
  );
}
