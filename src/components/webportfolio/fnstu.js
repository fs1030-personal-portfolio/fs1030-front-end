import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

export default function FN() {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardHeader
        title="FN Studios"
        subheader="Flash/XML/CMS"
      />
      <CardMedia
        className={classes.media}
        image="./assets/portfolio/fnstu.png"
        title="FN Studios"
        alt="FN Studios"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
            FN Studio was my personal website when flash was really big in the early 2000's I had a small team that did flash animations and games in flash.<br/><br/>
            <br/>
        </Typography>
      </CardContent>
    </Card>
  );
}
