import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import {Row} from 'reactstrap';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

export default function Websites() {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardHeader
        title="Other Webwork"
      />
      <CardMedia
        className={classes.media}
        image="https://res.cloudinary.com/practicaldev/image/fetch/s--b-n9DghP--/c_imagga_scale,f_auto,fl_progressive,h_420,q_auto,w_1000/https://dev-to-uploads.s3.amazonaws.com/uploads/articles/x1zussyb4hotcrnrnfss.png"
        title="Fussion-Wasteland"
        alt="Fussion-Wasteland"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
             This is a small list of websites I have collaborated on.<br/><br/>
            <Row>
            <ul>
              <li><a class="websitelinks"  target="_blank" rel="noopener noreferrer"  href="https://drbray.ca/">drbray.ca</a></li>
              <li><a class="websitelinks"  target="_blank" rel="noopener noreferrer" href="https://revstarconsulting.com/">revstarconsulting.com</a></li>
              <li><a class="websitelinks"  target="_blank" rel="noopener noreferrer" href="https://medzoomer.com/">medzoomer.com</a></li>
              <li><a class="websitelinks"  target="_blank" rel="noopener noreferrer" href="https://tealife.ca/">tealife.ca</a></li>
              <li><a class="websitelinks"  target="_blank" rel="noopener noreferrer" href="https://wellbebe.com/">wellbebe.com</a></li>
              <li><a class="websitelinks"  target="_blank" rel="noopener noreferrer" href="https://nutman.ca/">nutman.ca</a></li>
              <li><a class="websitelinks"  target="_blank" rel="noopener noreferrer" href="https://bumblebeebaskets.ca/">bumblebeebaskets.ca</a></li>
              <li><a class="websitelinks"  target="_blank" rel="noopener noreferrer" href="https://www.faircrestdoor.com/">faircrestdoor.com</a></li>
              <li><a class="websitelinks"  target="_blank" rel="noopener noreferrer" href="https://christyinsurance.com/">christyinsurance.com</a></li>
              <li><a class="websitelinks"  target="_blank" rel="noopener noreferrer" href="https://pigmentnpaper.com/">pigmentnpaper.com</a></li>
            </ul>
            <br/>
            </Row>
        </Typography>
      </CardContent>
    </Card>
  );
}
