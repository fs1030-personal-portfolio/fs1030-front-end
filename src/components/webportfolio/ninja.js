import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import YoutubeEmbed from '../YoutubeEmbed';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

export default function Ninja() {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardHeader
        title="Fussion-Ninja"
        subheader="Unity Engine web game with C#"
      />
      <CardMedia/>
          <YoutubeEmbed embedId="kIgp5Jn4kNQ" />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
            This was a small project I made in Unity to test how well web based video games would be in its engine this was circa 2013.<br/><br/><br/><br/>
            <br/>
        </Typography>
      </CardContent>
    </Card>
  );
}
