import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

export default function Erp() {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardHeader
        title="Odoo ERP system"
      />
      <CardMedia
        className={classes.media}
        image="./assets/portfolio/3.png"
        title="Odoo ERP system"
        alt="Odoo ERP system"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
            Using Odoo Studio I created a custom ERP system for one of Canada's largest E-juice companies It included a tracking system for Manufacturing with unique id's to assist in quality control and upholding ISO 9001 standard.<p/>The ERP solution also included day to day operations in 6 store fronts from cleaning to calibrating the nicotine machines & product warranty tracking<p/><br/><br/>
            <br/>
        </Typography>
      </CardContent>
    </Card>
  );
}
