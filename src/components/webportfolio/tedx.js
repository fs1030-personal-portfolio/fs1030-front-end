import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

export default function Tedx() {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardHeader
        title="TedX College Website"
        subheader="HTML/CSS/JS"
      />
      <CardMedia
        className={classes.media}
        image="./assets/portfolio/tedx.png"
        title="tedx"
        alt="tedx"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
            I created the first website for the TedX St. Lawrence College annual event that is put together by third year Business Administration students.<br/><br/>
            <br/>
        </Typography>
      </CardContent>
    </Card>
  );
}
