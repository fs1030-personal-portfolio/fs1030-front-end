import React, { useState } from "react";
import { useHistory } from "react-router";
import Container from "@material-ui/core/Container";
import {Form,ButtonToggle,Row,Col,FormGroup,Label,Input} from "reactstrap";

const EditBlog = (props) => {
  let id = props.match.params.id;
  let EditBlog = props.location.state;
  const history = useHistory();
  const token = sessionStorage.getItem("token");
  const [Blog, setBlog] = useState({
    Title: `${EditBlog.Title}`,
    subject: `${EditBlog.subject}`,
    content: `${EditBlog.content}`,
    VideoID: `${EditBlog.VideoID}`,
    imageLink: `${EditBlog.imageLink}`,
  });

  const handleSubmit = (event) => {
    event.preventDefault();
    fetch(`http://localhost:4000/blog/entries/${id}`, {
      method: "PATCH",
      headers: {
        Authorization: `Bearer ${token}`,
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(Blog),
    }).then((response) => response.json());
    history.push("/submissions");
  };

  const handleChange = (event) => {
    event.persist();
    setBlog((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.value,
    }));
  };

  return (
    <div className="main-panel">
        <Container  className="containerCU" fixed>
      <Row>
        <Col>
        <center><h3>You are Editing:<i>{Blog.Title}</i></h3></center><br/>
        </Col>
      </Row>
          <Form onSubmit={(e) => handleSubmit(e)}>
            <Row form>
              <Col md={6}>
                <FormGroup>
                  <Label>Title</Label>
                  <Input
                    type="text"
                    name="Title"
                    id="Title"
                    defaultValue={Blog.Title}
                    onChange={handleChange}
                  />
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <Label>subject</Label>
                  <Input
                    type="text"
                    name="subject"
                    id="subject"
                    defaultValue={Blog.subject}
                    onChange={handleChange}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row form>
              <Col md={6}>
                <FormGroup>
                  <Label>content</Label>
                  <Input
                    type="textarea"
                    name="content"
                    id="content"
                    defaultValue={Blog.content}
                    onChange={handleChange}
                  />
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <Label>VideoID</Label>
                  <Input
                    type="text"
                    name="VideoID"
                    id="VideoID"
                    defaultValue={Blog.VideoID}
                    onChange={handleChange}
                  />
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <Label>Image Link</Label>
                  <Input
                    type="text"
                    name="imageLink"
                    id="imageLink"
                    defaultValue={Blog.imageLink}
                    onChange={handleChange}
                  />
                </FormGroup>
              </Col>
            </Row>
            <ButtonToggle type="submit" color="warning">Submit</ButtonToggle>
          </Form>
        </Container>
    </div>
  );
};

export default EditBlog;
