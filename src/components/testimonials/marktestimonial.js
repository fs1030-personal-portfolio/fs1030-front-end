import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Button} from 'reactstrap';
import DRating from './Rating'

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

export default function RecipeReviewCardm() {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card className={classes.root}>
      <CardHeader
        avatar={
          <center><img class="testimonial" img src="./Assets/resume/intercall.png" alt="Intercall" /></center>
        }
        title="Mark B."
        subheader="Operation Manager - Intercall "
      />
      <CardMedia
        className={classes.media}
        image="./Assets/ref/mark.jpg"
        title="Mark B of Intercall Canada"
        alt="Mark B of Intercall Canada"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
            <i>&quot;David not only took the time to develop this tool that will benefit the agents,business and ultimately our clients, but also took the time to build some video and power point presentations to accompany the tool.&quot;</i><br/><br/>
            <center><DRating/></center>
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more">
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
              <center>
                  <Button color="warning"  target="_blank"  href="./Assets/ref/ICref2.jpg">Open Reference Letter</Button><br/><br/>
                  <a  href="./Assets/ref/ICref2.jpg" target="_blank" ><img class="ref" src="./Assets/ref/ICref2.jpg" alt="refernce letter from Intercall"/></a>
              </center>
        </CardContent>
      </Collapse>
    </Card>
  );
}
