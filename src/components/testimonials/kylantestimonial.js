import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Button} from 'reactstrap';
import DRating from './Rating'

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

export default function RecipeReviewCard() {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card className={classes.root}>
      <CardHeader
        avatar={
          <center><img class="testimonial" img src="./Assets/resume/VC.png" alt="Vapours Canada Logo"/></center>
        }
        title="Kylan S."
        subheader="CEO/Founder - Canada E-Juice"
      />
      <CardMedia
        className={classes.media}
        image="./Assets/ref/kylan.png"
        title="Kylan S of Vapours Canada"
        alt="Kylan S of Vapours Canada"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
            <i>&quot;You have gone above and beyong to assist us with many tasks to streamline production and efficiency of the stores and we appreciate all your contributions to the company&quot;</i><br/><br/>
            <br/><center><DRating/></center>
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more">
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
              <center>
                  <Button color="warning"  target="_blank"  href="./Assets/ref/VCref.png">Open Reference Letter</Button><br/><br/>
                  <a  href="./Assets/ref/VCref.png" target="_blank" ><img class="ref" src="./Assets/ref/VCref.png" alt="refernce letter from Intercall"/></a>
              </center>
        </CardContent>
      </Collapse>
    </Card>
  );
}
