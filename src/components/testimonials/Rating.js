import React from 'react';
import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';

export default function DRating() {
  const [value] = React.useState(4);

  return (
    <div>
      <Box component="fieldset" mb={3} borderColor="transparent">
        <Rating name="disabled" value={value} disabled />
      </Box>
    </div>
  );
}
