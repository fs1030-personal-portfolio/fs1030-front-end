import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Button} from 'reactstrap';
import DRating from './Rating'

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

export default function RecipeReviewCardl() {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card className={classes.root}>
      <CardHeader
        avatar={
          <center><img class="testimonial" img src="./Assets/resume/intercall.png" alt="Intercall" /></center>
        }
        title="Louisa A."
        subheader="Floor Supervisor - Intercall "
      />
      <CardMedia
        className={classes.media}
        image="./Assets/ref/louisa.webp"
        title="Louisa A. of Intercall Canada"
        alt="Louisa A. of Intercall Canada"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
            <i>&quot;The site he created will be greatly beneficial to my team,as it has everything my agents need to complete their job duties all in one easy to use and quick running product.&quot;</i><br/><br/>
            <br/><center><DRating/></center>
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton
          className={clsx(classes.expand, {
            [classes.expandOpen]: expanded,
          })}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more">
          <ExpandMoreIcon />
        </IconButton>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
              <center>
                  <Button color="warning"  target="_blank"  href="./Assets/ref/ICref.jpg">Open Reference Letter</Button><br/><br/>
                  <a  href="./Assets/ref/ICref.jpg" target="_blank" ><img class="ref" src="./Assets/ref/ICref.jpg" alt="refernce letter from Intercall"/></a>
              </center>
        </CardContent>
      </Collapse>
    </Card>
  );
}
