import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import YoutubeEmbed from './YoutubeEmbed';


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function ScrollableTabsButtonAuto() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
          aria-label="scrollable auto tabs example"
        >
          <Tab label="Twitch Promo" {...a11yProps(0)} />
          <Tab label="Death Cover" {...a11yProps(1)} />
          <Tab label="Compilation" {...a11yProps(2)} />
          <Tab label="Led Zeppelin Solo" {...a11yProps(3)} />
          <Tab label="Megadeth Song" {...a11yProps(4)} />
          <Tab label="Skid Row song" {...a11yProps(5)} />
          <Tab label="Megadeth Solo" {...a11yProps(6)} />
          <Tab label="Mägo de Oz Solo" {...a11yProps(7)} />
          <Tab label="Guns N Roses Solo" {...a11yProps(8)} />
          <Tab label="Van Halen Solo" {...a11yProps(9)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
      <YoutubeEmbed embedId="RH9NOV4SzPI" />
      </TabPanel>
      <TabPanel value={value} index={1}>
      <YoutubeEmbed embedId="vC-iJ_gc0AU" />
      </TabPanel>
      <TabPanel value={value} index={2}>
      <YoutubeEmbed embedId="LIRqMY3lMdY" />
      </TabPanel>
      <TabPanel value={value} index={3}>
      <YoutubeEmbed embedId="CPhWGkPCPHI" />
      </TabPanel>
      <TabPanel value={value} index={4}>
      <YoutubeEmbed embedId="8iUYg5q5-T4" />
      </TabPanel>
      <TabPanel value={value} index={5}>
      <YoutubeEmbed embedId="65gbp5ReZRA" />
      </TabPanel>
      <TabPanel value={value} index={6}>
      <YoutubeEmbed embedId="YFruV8-RmuQ" />
      </TabPanel>
      <TabPanel value={value} index={7}>
      <YoutubeEmbed embedId="B5MgDQc_TtA" />
      </TabPanel>
      <TabPanel value={value} index={8}>
      <YoutubeEmbed embedId="PtZYfOnTVo0" />
      </TabPanel>
      <TabPanel value={value} index={9}>
      <YoutubeEmbed embedId="w6b4-eWLfro" />
      </TabPanel>
    </div>
  );
}
