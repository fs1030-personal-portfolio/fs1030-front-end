import React, { useEffect, useState } from "react";
import parseJwt from "../../helpers/authHelper";
import { Container } from "reactstrap";
import { Table, Button, Row } from "reactstrap";
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";

const SubmissionsList = () => {
  const token = sessionStorage.getItem("token");
  const Submissions = parseJwt(token).Submissionsname;
  const [Submissionss, setSubmissionss] = useState([]);

  useEffect(() => {
    const getData = async () => {
      const response = await fetch("http://localhost:4000/contact_form/entries/", {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      const data = await response.json();
      setSubmissionss(data);
    };
    getData();
  }, [token]);
  
  

  const SubmissionsDelete = async (event, Submissions) => {
    event.preventDefault()
    console.log(Submissions)
    const response = await fetch(`http://localhost:4000/contact_form/entries/${Submissions.SubmissionID}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`,
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    })
    const data = await response.json();
    console.log(data)
    window.location.reload();
  }

  return (
    <Container className="mainContent">
      <Row className="SubmissionsTitle">
        <h2 className="display-5">
          Total Submissionss:{Submissionss.length}
          {Submissions}
        </h2>
      </Row>
      <Table responsive className="content">
        <thead>
          <tr>
            <th>Submissions name</th>
            <th>Password</th>
            <th>Email</th>
            <th>name</th>
          </tr>
        </thead>
        <tbody>
          {Submissionss.length === 0 && (
            <tr>
              <td colSpan="4" className="text-center">
                <i>No Submissionss found</i>
              </td>
            </tr>
          )}
          {Submissionss.length > 0 &&
            Submissionss.map((Submissions) => (
              <tr>
                <td>{Submissions.name}</td>
                <td>{Submissions.DiscordH}</td>
                <td>{Submissions.Email}</td>
                <td>{Submissions.Message}</td>
                <td>
                  {" "}
                  <Button color="danger" onClick={(e) => SubmissionsDelete(e, Submissions)}>
                    <DeleteForeverIcon />
                  </Button>
                </td>
              </tr>
            ))}
        </tbody>
      </Table>
    </Container>
  );
};

export default SubmissionsList;
