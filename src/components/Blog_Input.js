import React, { useState } from 'react'
import { Form, FormGroup, Col, Input, Label, Button, Container} from 'reactstrap'
import Tooltip from '@material-ui/core/Tooltip';
import Swal from 'sweetalert2'



const SQLDateParsed = () => {

    // MySQL formatted UTC timestamp 
    let d = new Date()
    let SQLDate = new Date(
    d.getFullYear(),
    d.getMonth(),
    d.getDate(),
    d.getHours(),
    (d.getMinutes()),
    d.getSeconds(),
    d.getMilliseconds()
    ).toISOString().slice(0, 19).replace('T', ' ')
    return(SQLDate)
    }


const BlogInput = () => {
    const [name, setName] = useState("")
    const [Title, setTitle] = useState("")
    const [content, setContent] = useState("")
    const [subject, setsubject] = useState("")
    const [VideoID, setVideoID] = useState("")
    const [imageLink, setimageLink] = useState("")
    const CreationDate =  SQLDateParsed();

    const formSubmit = async event => {
        event.preventDefault()
        const response = await fetch('http://localhost:4000/blog/entries', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({name,Title,subject,imageLink,content,CreationDate,VideoID})
        })
        const payload = await response.json()
        if (response.status >= 400) {
            alert(`Woopsie! Error: ${payload.message} for fields: ${payload.invalid.join(",")}`)
        } else {
            Swal.fire({
                icon: 'info',
                title: 'Success!',
                titleText: 'Success' ,
                text: 'A New Blog  Has been Created!',
                confirmButtonColor: '#4BB543',
              })
            resetForm()
        }
    }
    const resetForm = () => {
        setName("")
        setTitle("")
        setContent("")
        setimageLink("")
        setVideoID("")
    }
    return (
        <Container className="containerBlog">
          <center>
                <Form className="my-5" onSubmit={formSubmit}>
                <FormGroup row>
                    <Label for="imageLink" sm={2}>Banner URL</Label>
                    <Col sm={10}>
                    <Tooltip title="Enter the Direct URL to the image you would like on your blog post">
                    <Input type="imageLink" name="imageLink" id="imageLink" placeholder="URL must be direct ending in proper image format such as .png" required value={imageLink} onChange={e => setimageLink(e.target.value)}/>
                    </Tooltip>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="titleEntry" sm={2}>title</Label>
                    <Col sm={10}>
                    <Tooltip title="Enter The title Of The Blog Here">
                    <Input type="Title" name="Title" id="TitleEntry" placeholder="Enter the title of your blog" value={Title} onChange={e => setTitle(e.target.value)}/>
                    </Tooltip>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="subject" sm={2}>Subject</Label>
                    <Col sm={10}>
                    <Tooltip title="Enter The Subject Of Your Blog Here">
                    <Input name="subject" id="subject" placeholder="Enter The Subject of your blog" required value={subject} onChange={e => setsubject(e.target.value)}/>
                    </Tooltip>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="VidEntry" sm={2}>Video Link</Label>
                    <Col sm={10}>
                    <Tooltip title="Enter the Youtube Video Id into this field to post a video in the blog post">
                    <Input type="VideoID" name="VideoID" id="VideoID" placeholder="Enter video Embed Id into this field" required value={VideoID} onChange={e => setVideoID(e.target.value)}/>
                    </Tooltip>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="messageEntry" sm={2}>Message</Label>
                    <Col sm={10}>
                    <Tooltip title="Fill In The Content Of Your Blog Post Here">
                    <Input type="textarea" name="text" id="messageEntry" placeholder="Enter Your Blog Post"   required value={content} onChange={e => setContent(e.target.value)}/>
                    </Tooltip>
                    </Col>
                </FormGroup>
                <FormGroup check row>
                    <Col sm={{ size: 10, offset: 2 }}>
                    <center><Button color="warning" >Submit</Button></center>
                    </Col>
                </FormGroup>
            </Form>
          </center>
        </Container>
      )
    }

    
    export default BlogInput