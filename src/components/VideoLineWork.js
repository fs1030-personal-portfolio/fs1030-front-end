import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import YoutubeEmbed from './YoutubeEmbed';


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function ScrollableTabsButtonAuto() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
          aria-label="scrollable auto tabs example"
        >
          <Tab label="Digital Rockers Intro" {...a11yProps(0)} />
          <Tab label="Fussion-Networks Intro" {...a11yProps(1)} />
          <Tab label="Unreal Engine 4 Showcase" {...a11yProps(2)} />
          <Tab label="New Sub Wr4th TV" {...a11yProps(3)} />
          <Tab label="R&J's Radio Intro" {...a11yProps(4)} />
          <Tab label="Cruisers-help.net Intro" {...a11yProps(5)} />
          <Tab label="Cataclysm New Weapon Promo" {...a11yProps(6)} />
          <Tab label="Video Portfolio" {...a11yProps(7)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
      <YoutubeEmbed embedId="gKJpfuXFep4" />
      </TabPanel>
      <TabPanel value={value} index={1}>
      <YoutubeEmbed embedId="AGaJnwzTccM" />
      </TabPanel>
      <TabPanel value={value} index={2}>
      <YoutubeEmbed embedId="AwTSAdYpU-k" />
      </TabPanel>
      <TabPanel value={value} index={3}>
      <YoutubeEmbed embedId="BvOg3bpMc7c" />
      </TabPanel>
      <TabPanel value={value} index={4}>
      <YoutubeEmbed embedId="O6tMVxu7Y3w" />
      </TabPanel>
      <TabPanel value={value} index={5}>
      <YoutubeEmbed embedId="Lmt7FrNDRls" />
      </TabPanel>
      <TabPanel value={value} index={6}>
      <YoutubeEmbed embedId="V_-Ote61-WI" />
      </TabPanel>
      <TabPanel value={value} index={7}>
      <YoutubeEmbed embedId="PT6nK51wmGo" />
      </TabPanel>
    </div>
  );
}
