import React, { Component } from "react";
import sortBy from "lodash.sortby";
import Container from "@material-ui/core/Container";
import TextField from "@material-ui/core/TextField";
import { Button } from 'reactstrap'
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import SubdirectoryArrowRightIcon from "@material-ui/icons/SubdirectoryArrowRight";

class Main extends Component {
  state = {
    Tasks: [],
    newTask: ""
  };

  componentDidMount() {
    const Tasks = JSON.parse(localStorage.getItem("Tasks"));
    if (Tasks) this.setState({ Tasks });
  }

  newTask = () => {
    if (this.state.newTask === "") return false;
    const { Tasks } = this.state;
    const data = new Date();
    Tasks.push({
      description: this.state.newTask,
      status: false,
      conclusao: "Date: " + data.toLocaleString().substr(0, 16)
    });
    this.setState({ newTask: "" });
    this.updtSS(Tasks);
  };

  CompleteTask = Task => {
    const { Tasks } = this.state;
    const index = Tasks.indexOf(Task);
    const data = new Date();

    Tasks[index]["status"] = !Task["status"];

    //Creation Date
    Tasks[index]["conclusao"] = Tasks[index]["conclusao"].substr(0, 24);

    if (Task["status"]) {
      Tasks[index]["conclusao"] +=
        " - Completed: " + data.toLocaleString().substr(0, 16);
    }

    this.updtSS(Tasks);
  };

  deleteTask = Task => {
    const { Tasks } = this.state;
    if (window.confirm("Confirm Removal?")) {
      Tasks.splice(Tasks.indexOf(Task), 1);
      this.updtSS(Tasks);
    }
  };

  addSubTask = Task => {
    const SubTask = prompt("Name Of Subtask:");

    if (SubTask) {
      const { Tasks } = this.state;
      const i = Tasks.indexOf(Task);

      if (Array.isArray(Tasks[i]["subTasks"])) {
        Tasks[i]["subTasks"].push({
          description: SubTask,
          status: false
        });
      } else {
        Tasks[i]["subTasks"] = [
          {
            description: SubTask,
            status: false
          }
        ];
      }

      this.updtSS(Tasks);
    }
  };

  deleteSubTask = (Task, SubTask) => {
    if (window.confirm("Confirm Removal?")) {
      const { Tasks } = this.state;
      const i = Tasks.indexOf(Task);

      Tasks[i]["subTasks"].splice(
        Tasks[i]["subTasks"].indexOf(SubTask),
        1
      );

      this.updtSS(Tasks);
    }
  };

  completeSubTask = (Task, SubTask) => {
    const { Tasks } = this.state;
    const i = Tasks.indexOf(Task);
    const j = Tasks[i]["subTasks"].indexOf(SubTask);

    Tasks[i]["subTasks"][j]["status"] = !Tasks[i]["subTasks"][j]["status"];

    this.updtSS(Tasks);
  };

  editTask = Task => {
    const { Tasks } = this.state;
    const TaskEditada = prompt("Edit Entry", Task.description);
    if (TaskEditada) {
      Tasks[Tasks.indexOf(Task)]["description"] = TaskEditada;
      this.updtSS(Tasks);
    }
  };

  updtSS(Tasks) {
    Tasks = sortBy(Tasks, ["status"]);
    localStorage.setItem("Tasks", JSON.stringify(Tasks));
    this.setState({ Tasks });
  }

  render() {
    return (
      <Container component="main" maxWidth="sm" style={{ marginTop: "25px" }}>

        <TextField
          id="outlined-full-width"
          label="Todo List"
          placeholder="Enter a task"
          fullWidth
          margin="normal"
          variant="outlined"
          color="Yellow"
          value={this.state.newTask}
          onInput={e => this.setState({ newTask: e.target.value })}
          onKeyPress={e => {
            if (e.key === "Enter") this.newTask(e);
          }}
        />

        <center>
          <Button
          variant="outlined"
          color="warning"
          fullWidth
          onClick={this.newTask}
        >
          Submit Task
        </Button>
        </center>
          <hr class="yellow"/>
        <List component="nav" aria-labelledby="nested-list-subheader">
          {this.state.Tasks.map(Task => {
            return (
              <React.Fragment>
                <ListItem
                  button
                  onDoubleClick={() => this.editTask(Task)}
                >
                  <ListItemIcon>
                    <Checkbox
                      edge="start"
                      tabIndex={-1}
                      checked={Task["status"]}
                      onClick={() => this.CompleteTask(Task)}
                    />
                  </ListItemIcon>

                  <ListItemText
                    primary={Task["description"]}
                    secondary={Task["conclusao"]}
                  />

                  <ListItemSecondaryAction>
                    <IconButton edge="end" aria-label="delete">
                      <SubdirectoryArrowRightIcon
                        onClick={() =>
                          this.addSubTask(Task)
                        }
                      />
                    </IconButton>
                    <IconButton edge="end" aria-label="delete">
                      <DeleteIcon onClick={() => this.deleteTask(Task)} />
                    </IconButton>
                  </ListItemSecondaryAction>
                </ListItem>
                    
                {Array.isArray(Task["subTasks"]) &&
                  Task["subTasks"].map(SubTask => {
                    return (
                      <List component="div" disablePadding>
                        <ListItem button style={{ paddingLeft: "70px" }}>
                          <ListItemIcon>
                            <Checkbox
                              edge="start"
                              tabIndex={-1}
                              checked={SubTask["status"]}
                              onClick={() =>
                                this.completeSubTask(Task, SubTask)
                              }
                            />
                          </ListItemIcon>

                          <ListItemText primary={SubTask["description"]} />

                          <ListItemSecondaryAction>
                            <IconButton edge="end" aria-label="delete">
                              <DeleteIcon
                                onClick={() =>
                                  this.deleteSubTask(Task, SubTask)
                                }
                              />
                            </IconButton>
                          </ListItemSecondaryAction>
                        </ListItem> 
                      </List>
                    );
                  })}
              </React.Fragment>
            );
          })}
        </List>
      </Container>
    );
  }
}

export default Main;
