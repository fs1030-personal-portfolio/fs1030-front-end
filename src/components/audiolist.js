//array  for audio played through mp3 player on music page
//MUSIC IN THIS SECTION AND REPO ARE COPYRIGHT BY DAVID DUCHESNEAU-HART ©2021
const audioLists = [
    
    
      {
        name: "Little Man",
        singer: "David D",
        cover:
        "../Assets/music/recon.png",
        musicSrc:
        "../Assets/music/mp3s/littleman.mp3",
      },
      {
        name: "Hello World",
        singer: "David D",
        cover:
        "../Assets/music/recon.png",
        musicSrc:
        "../Assets/music/mp3s/helloworld.mp3",
      },
      {
        name: "Boom Stick",
        singer: "David D",
        cover:
        "../Assets/music/recon4.png",
        musicSrc:
        "../Assets/music/mp3s/boomstick.mp3",
      },
      {
        name: "CrystalFields",
        singer: "David D/Kyle B/ Devon K",
        cover:
        "../Assets/music/recon2.png",
        musicSrc:
        "../Assets/music/mp3s/crystalfields.mp3",
      },
      {
        name: "Valley Of Fire Improv",
        singer: "David D",
        cover:
        "../Assets/music/recon.png",
        musicSrc:
        "../Assets/music/mp3s/valley.mp3",
      },
      {
        name: "Carry On (Chad F Cover)",
        singer: "David D",
        cover:
        "../Assets/music/recon3.png",
        musicSrc:
        "../Assets/music/mp3s/carryon.mp3",
      },
      {
        name: "Abducted",
        singer: "David D",
        cover:
        "../Assets/music/recon5.png",
        musicSrc:
        "../Assets/music/mp3s/abducted.mp3",
      },
      {
        name: "Creed",
        singer: "David D",
        cover:
        "../Assets/music/recon5.png",
        musicSrc:
        "../Assets/music/mp3s/creed.mp3",
      },
      {
        name: "Twinkle Toes",
        singer: "David D",
        cover:
        "../Assets/music/recon5.png",
        musicSrc:
        "../Assets/music/mp3s/twinkle.mp3",
      },
      {
        name: "Lost In Space",
        singer: "David D",
        cover:
        "../Assets/music/recon5.png",
        musicSrc:
        "../Assets/music/mp3s/space.mp3",
      },
      {
        name: "Atmosphere",
        singer: "David D",
        cover:
        "../Assets/music/recon5.png",
        musicSrc:
        "../Assets/music/mp3s/atmosphere.mp3",
      }
  ];

  export default audioLists 