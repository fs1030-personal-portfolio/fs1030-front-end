import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

//show back hover effect for rocket acoustic 
const imageSelected = "./Assets/music/guitars/Aback.png";
const image = "./Assets/music/guitars/Afront.png";
const HoverImage = props => {
  function over(e) {
    e.currentTarget.src = props.hoverImage;
  }
  function out(e) {
    e.currentTarget.src = props.normalImage;
  }
  return <img alt={"Back and front of the acoustic guitar"} src={props.normalImage} onMouseOver={over} onMouseOut={out} />;
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
}));

export default function GuitarShowCase() {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <div className={classes.root}>
      <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1bh-content"
          id="panel1bh-header"
        >
          <Typography className={classes.heading}>Mr. Sparkles</Typography>
          <Typography className={classes.secondaryHeading}><img width="25" src="./Assets/music/Gicon.png" alt="" />Washburn KC44v</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
                    <center><img class="radis" width="600"  src="./Assets/music/guitars/Sparkles.png" alt="" /></center>
           <p className="display-5">Name: Mr. Sparkles<br/>
                                    Purchase Date: Summer 2014<br/>
                                    MFG Year: 1991</p>
           <hr className="my-2" />
           <p className="lead">I picked this guitar up in 2014 at a pawn shop I was suprised to see it there and for so cheap I paid $125 CDN for it and gave it some love and added some Vine Inlays to it. This is my favorite guitar to play on.</p>
           <hr className="my-2" />
           <p class="card-text">The Washburn KC44 was a Chicago Series superstrat electric guitar, similar to the KC40 but with HSH pickup configuration and 22 frets. The KC44 had an alder body with an arched top and back (a little like the Ibanez S-Series) with deeply scalloped cutaways. It came with a Washburn 600S Pro Floyd Rose tremolo. It came with a speckled rain finish in black or white or a natural finish.<br/></p>
           <p className="lead">
          </p>
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel2bh-content"
          id="panel2bh-header"
        >
          <Typography className={classes.heading}>HellSing</Typography>
          <Typography className={classes.secondaryHeading}>
          <img width="25" src="./Assets/music/Gicon.png" alt="" />FA Custom FrakenStrat
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
                    <center><img class="radis" width="600"  src="./Assets/music/guitars/EVH.png" alt="" /></center>
           <p className="display-5">Name: Hellsing<br/> 
                                    Purchase Date: Winter 2013<br/>
                                    MFG Year: 1991</p>
           <hr className="my-2" />
           <p className="lead">Originally this guitar was a first act guitar in all black that I purchased at a pawn shop for $25 CDN. I took it apart put in some new pickups/hardware & refinished the guitar. I had made a matching base for my Bassist at the time as well!</p>
           <hr className="my-2" />
           <p class="card-text">Dimarzio Red Velvet Pickups were installed with all Chrome hardware/switch/knobs and a completed gut and rewire of the picksup was done including a custom paint job EVH inspired.</p>
           
           <p className="lead">
          </p>
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion expanded={expanded === 'panel3'} onChange={handleChange('panel3')}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel3bh-content"
          id="panel3bh-header"
        >
          <Typography className={classes.heading}>Silver Surfer</Typography>
          <Typography className={classes.secondaryHeading}>
          <img width="25" src="./Assets/music/Gicon.png" alt="" />Epiphone Les paul Custom
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
                    <center><img class="radis" width="600"  src="./Assets/music/guitars/SilverS.png" alt="" /></center>
           <p className="display-5">Silver Surfer<br/>
                                    Purchase Date: Spring 2015<br/>
                                    MFG Year: 2012</p>
           <hr className="my-2" />
           <p className="lead">This guitar was purchase off of one of my Band mates in 2015 for $400 CDN he had added a Seymour Duncan APH-2 pickup into the bridge and the person before him had added a kill switch to it.</p>
           <hr className="my-2" />
           <p class="card-text">The new Les Paul Custom is part of Epiphone’s Inspired by Gibson Collection and honors the 1950s classic designed by Mr. Les Paul himself in 1954 as the “tuxedo” version of his groundbreaking solid body masterpiece. Featuring the traditional Custom bound all-mahogany body, 60s SlimTaper™ neck profile, ebony fingerboard with block inlays, Silver Epiphone bridge and Stop Bar tailpiece, a pair of critically acclaimed Epiphone ProBucker™ humbuckers, and Custom split-diamond inlay on the headstock. <br/></p>
           <p className="lead">
          </p>
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion expanded={expanded === 'panel31'} onChange={handleChange('panel31')}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel31bh-content"
          id="panel31bh-header"
        >
          <Typography className={classes.heading}>Slickback</Typography>
          <Typography className={classes.secondaryHeading}>
          <img width="25" src="./Assets/music/Gicon.png" alt="" />Schecter Diamond series omen 6 Satin Finish Red
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
                    <center><img class="radis" width="600"  src="./Assets/music/guitars/Slick.png" alt="" /></center>
           <p className="display-5">Name: Slickback<br/> 
                                    Purchase Date: Winter 2004<br/>
                                    MFG Year: 2003</p>
           <hr className="my-2" />
           <p className="lead">This is the Sister Guitar of Boy Blue. They were both purchased the same day this was my Mothers guitar when she used to play.</p>
           <hr className="my-2" />
           <p class="card-text">The Omen collection is not to be taken lightly. Featuring mahogany bodies, Schecter Diamond Plus pickups, 24 Frets, and a fitted contoured heel, these guitars can pack a serious punch while remaining extremely versatile and ready for the stage.<br/></p>
           <p className="lead">
          </p>
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion expanded={expanded === 'panel32'} onChange={handleChange('panel32')}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel32bh-content"
          id="panel32bh-header"
        >
          <Typography className={classes.heading}>Boy Blue</Typography>
          <Typography className={classes.secondaryHeading}>
          <img width="25" src="./Assets/music/Gicon.png" alt="" />Schecter Diamond series omen 6 Blue
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
                    <center><img class="radis" width="600"  src="./Assets/music/guitars/BB.png" alt="" /></center>
           <p className="display-5">Name: Boy Blue<br/> 
                                    Purchase Date: Winter 2004<br/>
                                    MFG Year: 2003</p>
           <hr className="my-2" />
           <p className="lead">This was the second guitar I ever owned and learned the most on it when I was a teenager, It is also the Brother of SlickBack the guitar my Mother played. I had sold it to my bandmate who sold me Silver Surfer in 2010 and re purchased it in 2016. I will never sell this guitar again!</p>
           <hr className="my-2" />
           <p class="card-text">The Omen collection is not to be taken lightly. Featuring mahogany bodies, Schecter Diamond Plus pickups, 24 Frets, and a fitted contoured heel, these guitars can pack a serious punch while remaining extremely versatile and ready for the stage.<br/></p>
           <p className="lead">
          </p>
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion expanded={expanded === 'panel33'} onChange={handleChange('panel33')}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel33bh-content"
          id="panel33bh-header"
        >
          <Typography className={classes.heading}>Tender Surrender</Typography>
          <Typography className={classes.secondaryHeading}>
          <img width="25" src="./Assets/music/Gicon.png" alt="" /> Paul Reed Smith Custom 24
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
                    <center><img class="radis" width="600"  src="./Assets/music/guitars/PRS.png" alt="" /></center>
           <p className="display-5">Name: Tender Surrender<br/> 
                                    Purchase Date: Winter 2020<br/>
                                    MFG Year: 2019</p>
           <hr className="my-2" />
           <p className="lead">I picked this guitar up in 2020 on a whim I was looking for a Jackson guitar with a floyd rose originally but ended up walking out with this instead, I paid approx $1100 CDN for this Guitar</p>
           <hr className="my-2" />
           <p class="card-text">The Custom 24 is the quintessential PRS guitar. This iconic instrument was the first model that Paul Reed Smith brought to the public at PRS Guitars’ first Winter NAMM show in 1985 and has been a top seller ever since. Played by internationally touring artists, gigging musicians, and aspiring players, the Custom 24 features PRS’s patented Gen III tremolo system and PRS 85/15 pickups with volume and tone controls and a 5-way blade switch. 85/15 pickups were personally designed by Paul Reed Smith to offer remarkable clarity and extended high and low end.</p>
           <p className="lead">
          </p>
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion expanded={expanded === 'panel34'} onChange={handleChange('panel34')}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel34bh-content"
          id="panel34bh-header"
        >
          <Typography className={classes.heading}>SnakeBite</Typography>
          <Typography className={classes.secondaryHeading}>
          <img width="25" src="./Assets/music/Cousticicon.png" alt="" />Rocket Electric Acoustic
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
                    <center><HoverImage normalImage={image} hoverImage={imageSelected} /></center>
           <p className="display-5">Name: SnakeBite<br/> 
                                    Purchase Date: Summer 2011<br/>
                                    MFG Year: 2012</p>
           <hr className="my-2" />
           <p className="lead">I picked this up when I was in kensington market with my fiance I dropped into this hole in the wall of a music store and purchased this unique guitar for $50 CDN after a bit of haggling! <br/> <br/> Hover over the image to see the Snakeskin Back</p>
           <hr className="my-2" />
           <p class="card-text">I cannot find any information about this guitar online so If anyone knows about where this came from or a MFG website please contact me.</p>
           <p className="lead">
          </p>
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion expanded={expanded === 'panel35'} onChange={handleChange('panel35')}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel35bh-content"
          id="panel35bh-header"
        >
          <Typography className={classes.heading}>Amps/Pedals Etc</Typography>
          <Typography className={classes.secondaryHeading}>
           <img width="25" src="./Assets/music/Aicon.png" alt="" /> Black Star/Boss/Digitech
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
                    <center><img class="radis" width="600"  src="./Assets/music/guitars/Boss.png" alt="" /></center>
           <p className="display-5">Name: Boss ME-80<br/> 
                                    Details: Mobile, battery powered, and filled with a diverse selection of flagship-quality BOSS amps and effects, the ME-80 is the ideal compact tone processor for performing guitarists. A friendly knob-based interface makes it simple to dial in great sounds in seconds, while easily selectable operation modes offer the flexibility of individual stompbox-style on/off or instant recall of complex multi-effects setups. Unique new footswitches deliver twice the control of previous designs for efficient and intuitive effects switching, patch selection, and real-time sound shaping while playing on stage. The free BOSS TONE STUDIO software unlocks even more tonal possibilities, providing a cool graphical interface for tweaking and organizing sounds on your computer, plus a web connection to BOSS TONE CENTRAL for direct access to free gig-ready patches created by top pro guitarists and much more.</p>
           <hr className="my-2" />
           <p/>
                    <center><img class="radis" width="600"  src="./Assets/music/guitars/DTDT.png" alt="" /></center>
           <p className="display-5">Name: Digitech Drop Tune <br/> 
                                    Details: The DigiTech® Drop is a dedicated polyphonic drop tune pedal that allows you to drop your tuning from one semitone all the way down to a full octave. Get down-tuned chunk without having to change guitars! The Drop also features a momentary/latching switch. With the switch set to momentary, you can turn the Drop into a true performance pedal. Fast trills and roller-coaster pitch dips are at the tip of your toes.</p>
           <hr className="my-2" />
           <p/>
                    <center><img class="radis" width="600"  src="./Assets/music/guitars/Amp.png" alt="" /></center>
           <p className="display-5">Name: Blackstar ID:260 TVP 2x60-watt 2x12"<br/> 
                                    Details: Blackstar's ID:260TVP guitar amplifier combo combines the feel, tone, and volume of a real tube amp with the convenience and flexibility only a solid state amp can provide. Blackstar calls it TVP (True Valve Power), and it lets you select what kind of tube tone you want, then save your custom sound for instant recall later. The ID:260TVP combo responds to your playing dynamics like a tube amp, breaking up when you dig in and easy to control with your volume knob.</p>
           <hr className="my-2" />
           <p/>
          </Typography>
        </AccordionDetails>
      </Accordion>
    </div>
  );
}
