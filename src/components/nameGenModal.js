import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import NameGen from  './NG/NGapp'
import { Button} from 'reactstrap';

function rand() {
  return Math.round(Math.random() * 1) - 1;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'relative',
    width: 600,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #336699',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function NG() {
  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const body = (
    <div style={modalStyle} className={classes.paper}>
      <p id="simple-modal-description">
          <br/>
          <center><img className="Appimg" src="Assets/portfolio/appBanners/GNG.gif" alt="Burning Guitar Animation that says Guitar Name Generator" /></center><br/>
          <p>Have you ever had the issue of not being able to come up with a name for your guitar? well look no further!</p>
          <h5>Click Randomize Below to start!</h5>
          <br/>
        <NameGen/> 
        <hr class="yellow"/>
          <br/>  
      </p>
    </div>
  );

  return (
    <div align="center">
      <Button type="button" color="warning"  onClick={handleOpen}>
        Guitar Name Generator
      </Button><br/><br/>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
}
