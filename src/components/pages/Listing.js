import React from 'react'
import { Container,Row,Col} from 'reactstrap'
import AdminPanel from '../Adminpanel';
import Pulse from 'react-reveal/Pulse';



const Listings = () => {
    return (
        <Container>
            <center><img className="bannerimg" src="Assets/top_login.png" alt="Cartoon of David with 2 computer monitors" /><br/><br/>
                <h1 className="display-3">Admin Section</h1></center><br/><br/>
                <Row>
                    <Col>
                        <Pulse>
                            <AdminPanel/> 
                        </Pulse>
                    </Col>
                </Row>
        </Container>
    )
}

export default Listings