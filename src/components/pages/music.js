import React from 'react';
import {Container} from 'reactstrap';
import "../../mp3player.css";
import VideoLineMusic from '../VideoLineMusic';
import GuitarShowCase from '../guitars/Guitarapp';
import Bounce from 'react-reveal/Bounce';
import Pulse from 'react-reveal/Pulse';

const Portfolio = () => {
    return (
        <Container>
            <main>
                <center><img className="bannerimg" src="Assets/music/musicbanner.gif" alt="are you ready to rock with recontastic Aka David D?" /></center><br/>
                <center><h1 className="display-3">Welcome To My Music Section</h1></center><br/>
                <center><p className="display-8"><br/>One of my biggest hobbies and passions besides technology is music  check out some of the highlights from my twitch stream below.<p/>I also have some original music that you can access by clicking the button in the top left corner to hear my originals!</p></center>
                        <Pulse>
                <center><h4 className="display-6"><br/><i>&quot;music is the universal language of mankind&quot;</i>- Wadsworth Longfellow </h4></center><br/>
                        </Pulse>
                   <center><h2 className="display-5">Rig Rundown</h2></center><br/>
                   <section>
                        <Pulse>
                            <GuitarShowCase/>  <br/><br/>
                        </Pulse>
                    </section>
                    <section>
                        <Bounce left>
                            <VideoLineMusic/><br/><br/>
                        </Bounce>
                    </section>
                
                <center><h4 className="display-6"><br/><i>&quot;When In Doubt just solo!&quot;</i>- David Duchesneau-Hart</h4></center><br/>
            </main>
        </Container>
        
    )
}

export default Portfolio