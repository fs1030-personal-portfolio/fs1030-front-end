import React, { useState } from 'react'
import { Form, FormGroup, Col, Input, Label, Button, Container} from 'reactstrap'
import Tooltip from '@material-ui/core/Tooltip';
import Swal from 'sweetalert2'

const SQLDateParsed = () => {

  // MySQL formatted UTC timestamp 
  let d = new Date()
  let SQLDate = new Date(
  d.getFullYear(),
  d.getMonth(),
  d.getDate(),
  d.getHours(),
  (d.getMinutes()),
  d.getSeconds(),
  d.getMilliseconds()
  ).toISOString().slice(0, 19).replace('T', ' ')
  return(SQLDate)
  }


const Contact = () => {
    const [name, setName] = useState("")
    const [Email, setEmail] = useState("")
    const [DiscordH, setDiscordH] = useState("")
    const [Message, setMessage] = useState("")
    const CreationDate =  SQLDateParsed();

    const formSubmit = async event => {
        event.preventDefault()
        const response = await fetch('http://localhost:4000/contact_form/entries', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({name,Email,DiscordH,Message,CreationDate})
        })
        const payload = await response.json()
        if (response.status >= 400) {
            alert(`Woopsie! Error: ${payload.Message} for fields: ${payload.invalid.join(",")}`)
        } else {
            Swal.fire({
                icon: 'info',
                title: 'Success!',
                titleText: 'Success' ,
                text: 'A Your Submission Has Been Sent To David D!',
                confirmButtonColor: '#4BB543',
              })
            resetForm()
        }
    }
    const resetForm = () => {
        setName("")
        setEmail("")
        setDiscordH("")
        setMessage("")
    }
    return (
        <Container>
            <main>
          <center>
            <center><img className="bannerimg" src="Assets/top.jpg" alt="Cartoon of David with 2 computer monitors" /></center><br/>
            <div class="backgroundjumbo">
                <Form className="my-5" onSubmit={formSubmit}>
                <FormGroup row>
                    <Label for="EmailEntry" sm={2}>Email</Label>
                    <Col sm={10}>
                    <Tooltip title="Enter your Email so you can be contacted">
                    <Input type="Email" name="Email" id="EmailEntry" placeholder="Enter Email to contact"  required value={Email} onChange={e => setEmail(e.target.value) }/>
                    </Tooltip>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="phoneEntry" sm={2}>Discord handle</Label>
                    <Col sm={10}>
                    <Tooltip title="Enter your discord handle if you have one">
                    <Input type="text" name="phone" id="phoneEntry" placeholder="Enter your discord handle enter N/A if you do not have one." value={DiscordH} onChange={e => setDiscordH(e.target.value)}/>
                    </Tooltip>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="nameEntry" sm={2}>Name</Label>
                    <Col sm={10}>
                    <Tooltip title="Enter your name so I know who I am speaking with">
                    <Input type="name" name="name" id="nameEntry" placeholder="Enter your full name" required value={name} onChange={e => setName(e.target.value)}/>
                    </Tooltip>
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="MessageEntry" sm={2}>Message</Label>
                    <Col sm={10}>
                    <Tooltip title="Type the Message you want to send to David D in here">
                    <Input type="textarea" name="text" id="MessageEntry" placeholder="Type the Message you want to send to David D Here"  required value={Message} onChange={e => setMessage(e.target.value)}/>
                    </Tooltip>
                    </Col>
                </FormGroup>
                <FormGroup check row>
                    <Col sm={{ size: 10, offset: 2 }}>
                    <center><Button color="warning" >Submit</Button></center>
                    </Col>
                </FormGroup>
            </Form>
            <a href="https://discord.gg/FGUaRYFmbM"><img className="discord" src="Assets/dis.png" alt="discord Icon"/></a> You can also contact me on discord directly @ recontastic#2514 Or <a href="https://discord.gg/FGUaRYFmbM"><b>join</b></a> my Discord Channel
            </div>
          </center>
            </main>
        </Container>
      )
    }

    
    export default Contact
