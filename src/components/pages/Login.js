import React, { useState } from 'react'
import { Container, Col, Row, Button, Form, FormGroup, Label, Input } from 'reactstrap'
import { useHistory, useLocation } from 'react-router-dom'
import Tooltip from '@material-ui/core/Tooltip';
import Swal from 'sweetalert2'

//Gandolf Hover Effect for eyes
const imageSelected = "Assets/no.png";
const image = "Assets/shallnotpass.png";
const HoverImage = props => {
  function over(e) {
    e.currentTarget.src = props.hoverImage;
  }
  function out(e) {
    e.currentTarget.src = props.normalImage;
  }
  return <img alt={"Cartoon of gandolf the great, with glowing red eyes when the mouse is hovered over his face"} src={props.normalImage} onMouseOver={over} onMouseOut={out} />;
};

const Login = () => {
    let history = useHistory();
    let location = useLocation();
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const alertContent = useState(null)

    const loginSubmit = async event => { 
        event.preventDefault()
        const response = await fetch('http://localhost:4000/auth', {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({email, password})
        })
        const payload = await response.json()
        if (response.status >= 400) {
            Swal.fire({
            icon: 'error',
            title: 'Oops...',
            titleText: 'Error' ,
            text: 'Something went wrong! The Password or Email is Incorrect.',
            confirmButtonColor: '#4BB543',
          })
        } else {
            sessionStorage.setItem('token', payload.token)
            let { from } = location.state || { from: { pathname: "/submissions" } }
            history.replace(from)
            window.location.reload()
        }
    }

    return (
      <main>
        <Container>
          <center>
        <div class="backgroundjumbo">
        <HoverImage normalImage={image} hoverImage={imageSelected} />
          <Form className="my-5" onSubmit={loginSubmit}>
            <div className={`alert ${!alertContent ? "hidden" : ""}`}>{alertContent}</div>
            <Row form>
              <Col md={6}>
                <FormGroup>
                  <Label for="usernameEntry">Email</Label>
                    <Tooltip title="Enter the email you used to register your account">
                      <Input type="text" name="username" id="usernameEntry" placeholder="Valid email address" value={email} onChange={e => setEmail(e.target.value)}/>
                    </Tooltip>
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <Label for="passwordEntry">Password</Label>
                    <Tooltip title="Enter the password you used when you registered your account">
                        <Input type="password" name="password" id="passwordEntry" placeholder="Valid password" value={password}  onChange={e => setPassword(e.target.value)}/>
                     </Tooltip>
                </FormGroup>
              </Col>
            </Row>
            <Button color="warning">Sign in</Button>
          </Form>
          </div>
          </center>
        </Container>
      </main>
    )
}

export default Login