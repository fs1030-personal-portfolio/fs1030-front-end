import React from 'react'
import { Container,Col,Card,Row } from 'reactstrap'
import Acclaim from '../resume/acclaimGames';
import Cast from '../resume/castHaste';
import CataclysmS from '../resume/cataclysmServers';
import Fussion from '../resume/fussionNetworks';
import Intercall from '../resume/intercallCanada';
import Travel from '../resume/travelCuts';
import Vapours from '../resume/vapoursCanada';
import Volunteer from '../resume/volunteerAbroad';
import Zoom from 'react-reveal/Zoom';
import Pulse from 'react-reveal/Pulse';

const Resume = () => {
    return (
        
        <Container>
            <main>
            <center><img className="bannerimg" src="Assets/top.jpg" alt="Cartoon of David with 2 computer monitors" /></center><br/>
                <center><h1 className="display-3">Resume</h1></center><br/>
                <br/>
                <center><h5>Currently Enrolled in - <a  without rel="noopener noreferrer" target="_blank" href="https://continue.yorku.ca/programs/certificate-in-full-stack-web-development/"><img className="york" src="Assets/resume/york.jpg" alt="York Univercity Logo" /></a></h5></center><br/>
            <section>
            <Row>
                <Col md="4" className="mb-5">
                    <Pulse>
                    <Card>
                        <Cast/>
                    </Card>
                    </Pulse>
                </Col>
                <Col md="4" className="mb-5">
                    <Pulse>
                    <Card>
                        <CataclysmS/>
                    </Card>
                    </Pulse>
                </Col>
                <Col md="4" className="mb-5">
                    <Pulse>
                    <Card>
                        <Vapours/>
                    </Card>
                    </Pulse>
                </Col>
                <Col md="4" className="mb-5">
                    <Zoom left cascade>
                    <Card>
                        <Intercall/>
                    </Card>
                    </Zoom>
                </Col>
                <Col md="4" className="mb-5">
                    <Zoom left cascade>
                    <Card>
                        <Fussion/>
                    </Card>
                    </Zoom>
                </Col>
                <Col md="4" className="mb-5">
                    <Zoom left cascade>
                    <Card>
                        <Acclaim/>
                    </Card>
                    </Zoom>
                </Col>
                <Col md="4" className="mb-5">
                    <Zoom left cascade>
                    <Card>
                        <Travel/>
                    </Card>
                    </Zoom>
                </Col>
                <Col md="4" className="mb-5">
                </Col>
                <Col md="4" className="mb-5">
                    <Zoom left cascade>
                    <Card>
                        <Volunteer/>
                    </Card>
                    </Zoom>
                </Col>
            </Row>
            <br/>
            </section>
            </main>
        </Container>
    )
}
export default Resume