import React from 'react'
import { Container, Row, Col, Button,Card} from 'reactstrap'
import YoutubeEmbed from '../YoutubeEmbed';
import RecipeReviewCard from '../testimonials/kylantestimonial'
import RecipeReviewCardm from '../testimonials/marktestimonial'
import RecipeReviewCardl from '../testimonials/louisatestimonial'
import Pulse from 'react-reveal/Pulse';
import Zoom from 'react-reveal/Zoom';

const Home = () => {
    return(
        <Container>
            <main>
            <section>
            <Row className="my-5">
                <Col lg="7">
                        <YoutubeEmbed embedId="30yAyAhgt1k" />
                </Col>
                <Col lg="5">
                    <Pulse>
                        <h1>David Duchesneau-Hart</h1><br/>
                        <p class="backgroundcontainer">I'm a tech passionate person, always willing to learn new things. I am an individual who seeks to find the most efficient way of completing my tasks & will go up and beyond what is expected to achieve the results.I have experience with Game developement/Modding and many things web oriented like website creation and ERP systems.</p><br/>
                        <Button color="warning" href="/contact">Contact Me today!</Button>
                    </Pulse>
                </Col>
            </Row>
                <br/>
                <center><h2>Testimonials</h2></center><br/>
            <Row>
            </Row>
            </section>
            <section>
            <Row>
                <Col md="4" className="mb-5">
                    <Zoom left cascade>
                        <Card>
                            <RecipeReviewCardm/>
                        </Card>
                    </Zoom>
                </Col>
                <Col md="4" className="mb-5">
                    <Zoom left cascade>
                        <Card>
                            <RecipeReviewCard/>
                        </Card>
                    </Zoom>
                </Col>
                <Col md="4" className="mb-5">
                    <Zoom left cascade>
                        <Card>
                            <RecipeReviewCardl/>
                        </Card>
                    </Zoom>
                </Col>
            </Row>
            </section>
            </main>
        </Container>
    )
}

export default Home