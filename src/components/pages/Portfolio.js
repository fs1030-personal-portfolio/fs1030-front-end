import React from 'react';
import {Card,Container,Row,Col} from 'reactstrap';
import VideoLineWork from '../VideoLineWork';
import Cataclysm from '../webportfolio/cataclysm'
import Wasteland from '../webportfolio/wasteland'
import Erp from '../webportfolio/erp'
import Dk from '../webportfolio/dk'
import Tedx from '../webportfolio/tedx'
import FN from '../webportfolio/fnstu'
import Lil from '../webportfolio/lilbidness'
import Ninja from  '../webportfolio/ninja'
import Arma from  '../webportfolio/arma'
import Websites from  '../webportfolio/Websites'
import Drawer from '../todomodal';
import NG from '../nameGenModal';
import OhmsCalc from '../ohmCalcModal';
import QuizApp from '../quizAppModal';
import Pulse from 'react-reveal/Pulse';
import Zoom from 'react-reveal/Zoom';
import Bounce from 'react-reveal/Bounce';

const Portfolio = () => {
    return (
        <Container>
            <main>
                <center><img className="bannerimg" src="Assets/top.jpg" alt="Cartoon of David with 2 computer monitors" /></center><br/>
                <center><h1 className="display-3">Work Portfolio</h1></center><br/>
            <section>
                <Row>
                    <Col>
                        <Pulse>
                            <Drawer/> 
                        </Pulse>
                    </Col>
                    <Col>
                        <Pulse>
                            <OhmsCalc/> 
                        </Pulse> 
                    </Col>
                    <Col>
                        <Pulse>
                            <NG/> 
                        </Pulse> 
                    </Col>
                    <Col>
                        <Pulse>
                            <QuizApp/> 
                        </Pulse> 
                    </Col>
                </Row>
                <br/>
            </section>
            <section>
            <Row>
                <Col md="4" className="mb-5">
                    <Pulse>
                    <Card>
                        <Cataclysm/>
                    </Card>
                    </Pulse>
                </Col>
                <Col md="4" className="mb-5">
                    <Pulse>
                    <Card>
                        <Wasteland/>
                    </Card>
                    </Pulse>
                </Col>
                <Col md="4" className="mb-5">
                    <Pulse>
                    <Card>
                        <Erp/>
                    </Card>
                    </Pulse>
                </Col>
                <Col md="4" className="mb-5">
                    <Zoom left cascade>
                    <Card>
                        <Dk/>
                    </Card>
                    </Zoom>
                </Col>
                <Col md="4" className="mb-5">
                    <Zoom left cascade>
                    <Card>
                        <Tedx/>
                    </Card>
                    </Zoom>
                </Col>
                <Col md="4" className="mb-5">
                    <Zoom left cascade>
                    <Card>
                        <FN/>
                    </Card>
                    </Zoom>
                </Col>
                <Col md="4" className="mb-5">
                    <Zoom left cascade>
                    <Card>
                        <Lil/>
                    </Card>
                    </Zoom>
                </Col>
                <Col md="4" className="mb-5">
                    <Zoom left cascade>
                    <Card>
                        <Ninja/>
                    </Card>
                    </Zoom>
                </Col>
                <Col md="4" className="mb-5">
                    <Zoom left cascade>
                    <Card>
                        <Arma/>
                    </Card>
                    </Zoom>
                </Col>
                <Col>
                </Col>
                <Col md="4" className="mb-5">
                    <Zoom left cascade>
                    <Card>
                        <Websites/>
                    </Card>
                    </Zoom>
                </Col>
                <Col>
                </Col>
            </Row>
            </section>
            <br/>
            <center><h1 className="display-3">Video Work</h1></center><br/>
            <section>
                <Bounce left>
                    <VideoLineWork/><br/><br/>
                </Bounce>
            </section>
            </main>
        </Container>
    )
}

export default Portfolio