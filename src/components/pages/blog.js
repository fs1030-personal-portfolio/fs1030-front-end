import React, { useEffect, useState } from 'react'
import { Container,Col} from 'reactstrap'
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/Favorite';
import CardActions from '@material-ui/core/CardActions';
import ShareIcon from '@material-ui/icons/Share';
import Bounce from 'react-reveal/Bounce';
import YoutubeEmbed from '../YoutubeEmbedblog';
import moment from "moment";

const useStyles = makeStyles({
    root: {
      maxWidth: 845,
    },
    media: {
      height: 240,
    },
  });


const BlogSubmissionsList = () => {
    const token = sessionStorage.getItem('token')
    const [listing, setListing] = useState([])
    const classes = useStyles();

    useEffect(() => {
        const getData = async () => {
            const response = await fetch('http://localhost:4000/blog/entries', {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            })
            const data = await response.json()
            setListing(data)
        }
        getData()
    }, [token])
    return (
        <Container>
            <main>
            <center><img className="bannerimg" src="Assets/top.jpg" alt="Cartoon of David with 2 computer monitors" /><br/>
                <h1 className="display-3">Welcome To my Blog</h1><br/></center>
                    {listing.length === 0 &&
                        <i>No Blog Posts found/Please make sure RestAPI is running</i>
                    }
                    {listing.length > 0 &&
                        listing.map(entry => 
                          <section>
                            <Bounce left>
                              <Col align="center">
                              <Card className={classes.root}>
                              <CardActionArea>
                                <CardMedia
                                  className={classes.media}
                                  image= {entry.imageLink}
                                  title= {entry.Title}
                                />
                                <CardContent>
                                  <Typography gutterBottom variant="h5" component="h2">
                                  {entry.Title}<br/>
                                  </Typography>
                                  <Typography variant="body2" color="textSecondary" component="p">
                                  {moment(entry.CreationDate).format("YYYY-MM-DD")}<br/><br/>
                                  {entry.content}
                                  </Typography>
                                  <YoutubeEmbed embedId={entry.VideoID} />
                                </CardContent>
                              </CardActionArea>
                              <CardActions>
                                <IconButton aria-label="add to favorites">
                                    <FavoriteIcon />
                                </IconButton>
                                <IconButton aria-label="share">
                                    <ShareIcon />
                                    <br/>
                                </IconButton>
                                {entry.subject}
                              </CardActions>
                              </Card>
                              </Col>
                              </Bounce>
                            <br/>
                            <br/>
                          </section>)
                    }
               
            </main>
        </Container>
        
    )
}

export default BlogSubmissionsList



