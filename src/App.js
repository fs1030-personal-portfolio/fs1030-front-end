import React from 'react'
import './App.css'
import Navigation from './components/shared/Navigation'
import Footer from './components/shared/footer'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Home from './components/pages/Home'
import Contact from './components/pages/Contact'
import Login from './components/pages/Login'
import Listing from './components/pages/Listing'
import Resume from './components/pages/Resume'
import Portfolio from './components/pages/Portfolio'
import music from './components/pages/music'
import fishroom from './components/pages/fishroom'
import secretpage from './components/pages/secretpage'
import PrivateRoute from './components/shared/PrivateRoute'
import NotFound from './components/404';
import BlogSubmissionsList from './components/pages/blog';
import EditUser from './components/userlist/User_Profile'
import EditBlog from './components/Blog_Profile'

function App() {
  return (
   <BrowserRouter>
        <Navigation />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/contact" component={Contact} />
          <Route exact path="/Resume" component={Resume} />
          <Route exact path="/Portfolio" component={Portfolio} />
          <Route exact path="/music" component={music} />
          <Route exact path="/fishroom" component={fishroom} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/secretpage" component={secretpage} />
          <Route exact path="/blog" component={BlogSubmissionsList} />
          <PrivateRoute path="/submissions">
            <Listing />
          </PrivateRoute>
            <PrivateRoute component={EditUser} exact path="/users/:id" />
            <PrivateRoute component={EditBlog} exact path="/blog/entries/:id" />
          <Route path="*" component={NotFound} />
        </Switch>
        <Footer />  
    </BrowserRouter>
  )
}

export default App;
